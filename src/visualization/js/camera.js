// Global objects for cameras and the cameraCenter, the grid position the main camera looks at
var camera, compassCamera, cameraCenter;

/**
 * Will create the camera and set initial camera parameters.
 * IMPORTANT: Width and height are here not known and thus set to arbitrary values,
 * they are overridden immediately after the dom is loaded and the real size is known.
 */
function initCamera() {
  // Size is overridden later
  var width = 100;
  var height = 100;
  camera = new THREE.PerspectiveCamera(70, width / height, 1, 100000);
  resetCameraPosition();

  compassCamera = new THREE.OrthographicCamera(-width / 2, width / 2, -height / 2, height / 2, 0.05, 100);
  compassCamera.position.set(0, -1, 0);
  compassCamera.up.set(0, 0, 1);
  compassCamera.lookAt(compassScene.position);
}

/**
 * Sets the camera to a nice starting position
 */
function resetCameraPosition() {
  var zoomLevel = 3;
  camera.position.set(0, -140 * 2 + zoomLevel * 20, 40 * 2 - zoomLevel * 6);
  camera.up.set(0, 0, 1);
  camera.lookAt(scene.position);
  cameraCenter = new THREE.Vector3();
}

/**
 * Move the camera so that it looks at [newGridPos]
 */
function setCamera(newGridPos) {
  // Move camera according to camera center change
  controls.target.copy(project.terrain.heightmap.gridCoordinatesToWorldCoordinates(newGridPos).setZ(0));
  camera.position.add(controls.target.clone().sub(cameraCenter));
  cameraCenter.copy(controls.target);

  // Update orbit controls
  controls.update();
}
