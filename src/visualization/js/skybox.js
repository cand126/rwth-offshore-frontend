// Global object holding the skybox
var skybox;

/**
 * Creates the skybox object and entity, always centered at the camera target position
 */
function initSkybox() {
  // All of this could be done with scene.background = new THREE.CubeTextureLoader().load(...),
  // but this way we can adjust the translation and rotation etc
  // https://threejs.org/docs/#api/loaders/CubeTextureLoader

  var textureURLs = ['images/sea_ft.JPG', 'images/sea_bk.JPG', 'images/sea_up.JPG', 'images/sea_dn.JPG', 'images/sea_rt.JPG', 'images/sea_lf.JPG'];

  loadTextures(textureURLs).catch(console.err).then(textures => {
    var materials = textures.map(texture => new THREE.MeshBasicMaterial({
      side: THREE.BackSide,
      map: texture,
      fog: false,
      depthWrite: false
    }));

    // Create outer skybox entity, that moves with the camera
    skybox = createEntity(['Transform', 'MoveWithCamera'], new THREE.Object3D(), {}).addToScene();

    // Add a child with the skybox mesh that is moved slightly downwards (so we do not see the mountains on this skyboxes horizon)
    createEntity(['Transform'], new THREE.Mesh(new THREE.CubeGeometry(1, 1, 1), materials), {
      'Transform': {
        position: new THREE.Vector3(0, 0, -1000),
        scale: new THREE.Vector3(100000, 100000, 100000),
        rotation: new THREE.Euler(0.5 * Math.PI, 0, 0)
      }
    }).addToObject(skybox);
  });
}
