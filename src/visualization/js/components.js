/**
 * Holds the entities transform.
 * 
 * NEEDED FOR EVERY ENTITY THAT IS IN THE SCENE GRAPH
 */
const TransformComponent = {
  name: 'Transform',
  state: {
    position: new THREE.Vector3(),
    scale: new THREE.Vector3(1, 1, 1),
    rotation: new THREE.Euler()
  }
};
entityManager.addComponent(TransformComponent.name, TransformComponent);

/**
 * Holds a grid position, overwrites the x/y position in Transform.
 * 
 * heightMode: Transform.z is...
 *   left untouched for 'untouched'
 *   zero for 'zero'
 *   (zero for heights below zero and the grid height for 'elevateOnLand') + heightOffset
 *   (the grid height for 'gridHeight') + heightOffset
 */
const GridPositionComponent = {
  name: 'GridPosition',
  state: {
    position: new THREE.Vector2(),
    heightMode: 'untouched', // zero, untouched, elevateOnLand, gridHeight
    heightOffset: 0.0
  }
};
entityManager.addComponent(GridPositionComponent.name, GridPositionComponent);

/**
 * Sets GridPosition to the nearest grid position of the mouse projection ray intersected with
 * the ground/ocean level
 *
 * lockOnHoveredObjects: Also intersection with Selectable objects and if intersected, use their grid position
 */
const GridPositionByMousePositionComponent = {
  name: 'GridPositionByMousePosition',
  state: {
    lockOnHoveredObjects: false
  }
};
entityManager.addComponent(GridPositionByMousePositionComponent.name, GridPositionByMousePositionComponent);

/**
 * Adds position, scale and rotation vectors, multiplied by the elapsed time, to the respective vectors in Transform
 */
const TransformAnimationComponent = {
  name: 'TransformAnimation',
  state: {
    position: new THREE.Vector3(),
    scale: new THREE.Vector3(),
    rotation: new THREE.Euler()
  }
};
entityManager.addComponent(TransformAnimationComponent.name, TransformAnimationComponent);

/**
 * Moves the texture by uv coordinates delta, multiplied by elapsed time
 */
const TextureAnimationComponent = {
  name: 'TextureAnimation',
  state: {
    delta: new THREE.Vector2(),
  }
};
entityManager.addComponent(TextureAnimationComponent.name, TextureAnimationComponent);

/**
 * Moves the texture of the ocean plane with the wind direction. The plane has to lie face up without rotation
 *
 * Needs TextureAnimation
 */
const MoveTextureWithWindComponent = {
  name: 'MoveTextureWithWind',
  state: {}
};
entityManager.addComponent(MoveTextureWithWindComponent.name, MoveTextureWithWindComponent);

/**
 * Flag whether the entity should rotate with the global windspeed or its live data
 *
 * Combine with RotateWithWindspeed
 */
const RotateWithGlobalWindspeedComponent = {
  name: 'RotateWithGlobalWindspeed',
  state: {}
};
entityManager.addComponent(RotateWithGlobalWindspeedComponent.name, RotateWithGlobalWindspeedComponent);

/**
 * Rotates the object continually around rotationAxis with speed (wind speed in m/s). The rotation speed is computed
 * with the characteristic curve for windmills
 *
 * Needs TransformAnimation
 */
const RotateWithWindspeedComponent = {
  name: 'RotateWithWindspeed',
  state: {
    speed: 0,
    rotationAxis: 'y'
  }
};
entityManager.addComponent(RotateWithWindspeedComponent.name, RotateWithWindspeedComponent);

/**
 * Aligns the entity around rotationAxis with the global wind direction (if live data is not enabled)
 */
const RotateWithWindDirectionComponent = {
  name: 'RotateWithWindDirection',
  state: {
    rotationAxis: 'z'
  }
};
entityManager.addComponent(RotateWithWindDirectionComponent.name, RotateWithWindDirectionComponent);

/**
 * Flag whether the entity should cast shadows
 */
const CastShadowComponent = {
  name: 'CastShadow',
  state: {}
};
entityManager.addComponent(CastShadowComponent.name, CastShadowComponent);

/**
 * Flag whether the entity should receive shadows
 */
const ReceiveShadowComponent = {
  name: 'ReceiveShadow',
  state: {}
};
entityManager.addComponent(ReceiveShadowComponent.name, ReceiveShadowComponent);

/**
 * Flag whether the entity can be moved in the respective editing mode
 */
const MovableComponent = {
  name: 'Movable',
  state: {}
};
entityManager.addComponent(MovableComponent.name, MovableComponent);

/**
 * Flag whether the entity can be selected in the camera editing mode
 */
const SelectableComponent = {
  name: 'Selectable',
  state: {
    selected: false
  }
};
entityManager.addComponent(SelectableComponent.name, SelectableComponent);

/**
 * Flag whether a cable can be attached to the entity
 */
const ConnectableComponent = {
  name: 'Connectable',
  state: {
    id: -1
  }
};
entityManager.addComponent(ConnectableComponent.name, ConnectableComponent);

/**
 * A Line (plane facing upwards) connecting the two world space points PointA and pointB
 */
const LineComponent = {
  name: 'Line',
  state: {
    pointA: new THREE.Vector3(),
    pointB: new THREE.Vector3()
  }
};
entityManager.addComponent(LineComponent.name, LineComponent);

/**
 * Cable connecting entities with the end point entities pointA and pointB
 *
 * Needs Line
 */
const CableConnectionComponent = {
  name: 'CableConnection',
  state: {
    pointA: -1,
    pointB: -1
  }
};
entityManager.addComponent(CableConnectionComponent.name, CableConnectionComponent);

/**
 * Holds filenames for a mesh and a material file, will be loaded automatically
 */
const MeshComponent = {
  name: 'Mesh',
  state: {
    filenameMesh: '',
    filenameMaterial: ''
  }
};
entityManager.addComponent(MeshComponent.name, MeshComponent);

/**
 * Creates a 1x1 plane with 1x1 elements 
 */
const PlaneComponent = {
  name: 'Plane',
  state: {}
};
entityManager.addComponent(PlaneComponent.name, PlaneComponent);

/**
 * Creates a radius=1 circle with 16 segments
 */
const CircleComponent = {
  name: 'Circle',
  state: {}
};
entityManager.addComponent(CircleComponent.name, CircleComponent);

/**
 * Creates a 1x1x1 box
 */
const BoxComponent = {
  name: 'Box',
  state: {}
};
entityManager.addComponent(BoxComponent.name, BoxComponent);

/**
 * Sets the material properties color, transparent and opacity of ALL CHILDREN with a material property
 */
const MaterialComponent = {
  name: 'Material',
  state: {
    color: new THREE.Color(0),
    transparent: false,
    opacity: 1.0
  }
};
entityManager.addComponent(MaterialComponent.name, MaterialComponent);

/**
 * Holds information about a windmill
 */
const WindmillComponent = {
  name: 'Windmill',
  state: {
    efficiency: 0.5
  }
};
entityManager.addComponent(WindmillComponent.name, WindmillComponent);

/**
 * Holds information about a power station
 */
const PowerstationComponent = {
  name: 'Powerstation',
  state: {
    efficiency: 0.7
  }
};
entityManager.addComponent(PowerstationComponent.name, PowerstationComponent);

/**
 * This will change the Transform such that the entity is scaled and moved to extend from the grid height to the lowest grid point in
 * the neighborhood. Used for windmill tower foot
 */
const ExtendToGroundComponent = {
  name: 'ExtendToGround',
  state: {}
};
entityManager.addComponent(ExtendToGroundComponent.name, ExtendToGroundComponent);

/**
 * Sets the transform to the camera look at target
 */
const MoveWithCameraComponent = {
  name: 'MoveWithCamera',
  state: {}
};
entityManager.addComponent(MoveWithCameraComponent.name, MoveWithCameraComponent);

/**
 * Sets the transform to the screen position [position] unprojected by the camera to a distance of 0.5.
 * Used for the compass
 */
const RelativeToCameraComponent = {
  name: 'RelativeToCamera',
  state: {
    position: new THREE.Vector2()
  }
};
entityManager.addComponent(RelativeToCameraComponent.name, RelativeToCameraComponent);

/**
 * Rotates the entity around rotationAxis by the negative camera z rotation. Used to rotate the compass, which lives in the separate space
 * so that it always points north
 */
const RotateWithCameraDirectionComponent = {
  name: 'RotateWithCameraDirection',
  state: {
    rotationAxis: 'z'
  }
};
entityManager.addComponent(RotateWithCameraDirectionComponent.name, RotateWithCameraDirectionComponent);

/**
 * Sets the color in Material to green if an object can be placed at GridPosition, red otherwise
 *
 * Needs GridPosition, Material
 */
const ColorCodeObjectPlaceabilityComponent = {
  name: 'ColorCodeObjectPlaceability',
  state: {}
};
entityManager.addComponent(ColorCodeObjectPlaceabilityComponent.name, ColorCodeObjectPlaceabilityComponent);

/**
 * Indicates that no entity can be placed at a grid position within a [distance] radius. Can be disabled during placement of
 * the windmill in its editing mode
 */
const RestrictedAreaWithinDistanceComponent = {
  name: 'RestrictedAreaWithinDistance',
  state: {
    distance: 10,
    enabled: true
  }
};
entityManager.addComponent(RestrictedAreaWithinDistanceComponent.name, RestrictedAreaWithinDistanceComponent);

/**
 * Holds a wind distribution (angle/wind speed pairs) to be shown on the windmill foot
 */
const WindDistributionComponent = {
  name: 'WindDistribution',
  state: {
    data: []
  }
};
entityManager.addComponent(WindDistributionComponent.name, WindDistributionComponent);

/**
 * Holds a live data id
 */
const LiveDataComponent = {
  name: 'LiveData',
  state: {
    id: ''
  }
};
entityManager.addComponent(LiveDataComponent.name, LiveDataComponent);
