/**
 * Creates the compass entity. It consists of the a group relative to the compass camera
 * The group contains the compass body mesh, the needle that points north (positive y axis)
 * and the weather vane which rotates with the wind direction
 */
function createCompass() {
  var compassScale = new THREE.Vector3(0.15, 0.15, 0.15);
  var compassPosition = new THREE.Vector2(-0.8, 0.7);

  // Compass container, sets the position relative to the camera
  var compassContainerEntity = createEntity(['Transform', 'RelativeToCamera'], new THREE.Object3D(), {
    'Transform': {
      position: new THREE.Vector3(0, 1, 0),
      scale: compassScale,
      rotation: new THREE.Euler(0.8, 0.3, 0)
    },
    'RelativeToCamera': {
      position: compassPosition // Screen position, will be unprojected to world space
    }
  });

  // Compass mesh
  createEntity(['Transform', 'Mesh'], new THREE.Object3D(), {
    'Mesh': {
      filenameMesh: 'models/kompass.obj',
      filenameMaterial: 'models/kompass.mtl'
    },
    'Transform': {
      position: new THREE.Vector3(0, 0, -1),
      rotation: new THREE.Euler(0, 0, Math.PI / 4.0)
    }
  }).addToObject(compassContainerEntity);

  // Needle
  createEntity(['Transform', 'Mesh', 'RotateWithCameraDirection'], new THREE.Object3D(), {
    'Mesh': {
      filenameMesh: 'models/nadel.obj',
      filenameMaterial: 'models/nadel.mtl'
    },
    'Transform': {
      position: new THREE.Vector3(0, 0, -1)
    }
  }).addToObject(compassContainerEntity);
  var weatherVaneGroup = createEntity(['Transform'], new THREE.Object3D(), {
    'Transform': {
      rotation: new THREE.Euler(0, 0, Math.PI / 2)
    }
  }).addToObject(compassContainerEntity);

  // Weather vane
  createEntity(['Transform', 'Mesh', 'RotateWithWindDirection'], new THREE.Object3D(), {
    'Mesh': {
      filenameMesh: 'models/flag.obj',
      filenameMaterial: 'models/flag.mtl'
    },
    'Transform': {
      position: new THREE.Vector3(0, 0, 0),
      scale: new THREE.Vector3(0.5, 0.5, 0.5)
    }
  }).addToObject(weatherVaneGroup);
  compassScene.add(compassContainerEntity);
  compass = compassContainerEntity;
}
