/**
 * Applies some of the settings from globalSettings. E.g. change visibility of objects or menus
 */
var SettingsSystem = function(entityManager) {
  this.entityManager = entityManager;
};
SettingsSystem.prototype.update = function(dt) {
  if (ground) {
    ground.visible = globalSettings.groundVisible;
  }
  if (seaplane) {
    seaplane.visible = globalSettings.seaplaneVisible;
  }
  if (stats) {
    if (globalSettings.fpsWindowEnabled) {
      stats.dom.classList.add("visible");
    } else {
      stats.dom.classList.remove("visible");
    }
  }
  if (document.querySelector('.dg.ac')) {
    if (globalSettings.extraControlsMenuEnabled) {
      document.querySelector('.dg.ac').style.display = "block";
    } else {
      document.querySelector('.dg.ac').style.display = "none";
    }
  }
};
entityManager.addProcessor(new SettingsSystem(entityManager));

/**
 * See components.js
 */
var GridPositionSystem = function(entityManager) {
  this.entityManager = entityManager;
};
GridPositionSystem.prototype.update = function(dt) {
  var gridPositionedObjects = this.entityManager.getAllComponentsData(['Transform', 'GridPosition']);
  for (var i = 0; i < gridPositionedObjects.length; i++) {
    var gridPositionedObject = gridPositionedObjects[i];
    var worldCoordinates = project.terrain.heightmap.gridCoordinatesToWorldCoordinates(gridPositionedObject.GridPosition.position);
    if (gridPositionedObject.GridPosition.heightMode == 'zero') {
      worldCoordinates.z = 0;
    } else if (gridPositionedObject.GridPosition.heightMode == 'untouched') {
      worldCoordinates.z = gridPositionedObject.Transform.position.z;
    } else if (gridPositionedObject.GridPosition.heightMode == 'elevateOnLand') {
      worldCoordinates.z = Math.max(worldCoordinates.z, 0) + gridPositionedObject.GridPosition.heightOffset;
    } else if (gridPositionedObject.GridPosition.heightMode == 'gridHeight') {
      worldCoordinates.z = worldCoordinates.z + gridPositionedObject.GridPosition.heightOffset;
    }
    gridPositionedObject.Transform.position.copy(worldCoordinates);
  }
};
entityManager.addProcessor(new GridPositionSystem(entityManager));

/**
 * See components.js
 */
var ExtendToGroundSystem = function(entityManager) {
  this.entityManager = entityManager;
};
ExtendToGroundSystem.prototype.update = function(dt) {
  var positionedObjects = this.entityManager.getAllComponentsData(['Transform', 'ExtendToGround']);
  for (var i = 0; i < positionedObjects.length; i++) {
    var positionedObject = positionedObjects[i];
    var topExtend = 0.1;

    // Get world position of object
    var objWorldPos = new THREE.Vector3().setFromMatrixPosition(sceneGraphEntities[positionedObject.entityId].matrixWorld);
    var objGridPos = project.terrain.heightmap.worldCoordinatesToGridCoordinates(objWorldPos);
    var gridHeight = project.terrain.heightmap.getGridHeight(objGridPos);
    var minGroundHeight = project.terrain.heightmap.getGridMinInNeighborhood(objGridPos);
    // We are in a local coordinate system where the object is placed below the windmill, which can be standing e.g. on a rock. Thus the total
    // height of the object might have to be increased
    var windmillHeightAtGridPos = Math.max(gridHeight, 0);
    var totalHeight = windmillHeightAtGridPos - minGroundHeight;


    positionedObject.Transform.scale.z = totalHeight + topExtend;
    positionedObject.Transform.position.z = -totalHeight / 2.0 + topExtend;

    // var worldCoordinates = project.terrain.heightmap.gridCoordinatesToWorldCoordinates(gridPositionedObject.GridPosition.position);
    // gridPositionedObject.Transform.position.copy(worldCoordinates);
  }
};
entityManager.addProcessor(new ExtendToGroundSystem(entityManager));

/** 
 * Set field of every child to value
 */
function allChildrenSet(node, field, value) {
  // node[field] = value;
  // if (node.children) {
  //   for (var i = 0; i < node.children.length; i++) {
  //     allChildrenSet(node.children[i], field, value);
  //   }
  // }
  node.traverse(function(child) {
    child[field] = value;
  });
}

/**
 * Updates the scene graph for the scene graph objects the entities are attached to. Is called as one of the very last systems, as the other systems write to some of the components
 * that are applied here
 *
 * Applies the transform parameters of Transform
 * Applies properties castShadow, receiveShadow
 * Applies material properties of Material
 */
var SceneGraphUpdateSystem = function(entityManager) {
  this.entityManager = entityManager;
};
SceneGraphUpdateSystem.prototype.update = function(dt) {
  var transformableObjects = this.entityManager.getAllComponentsData(['Transform']);
  for (var i = 0; i < transformableObjects.length; i++) {
    var transformableObject = transformableObjects[i];

    // Apply transform to scene graph object
    if (sceneGraphEntities.hasOwnProperty(transformableObject.entityId)) {
      sceneGraphEntities[transformableObject.entityId].position.copy(transformableObject.Transform.position);
      sceneGraphEntities[transformableObject.entityId].scale.copy(transformableObject.Transform.scale);
      sceneGraphEntities[transformableObject.entityId].rotation.copy(transformableObject.Transform.rotation);
    }
  }

  // Reset castShadow, receiveShadow
  for (var i = 0; i < this.entityManager.entities.length; i++) {
    var entityId = this.entityManager.entities[i];
    if (sceneGraphEntities.hasOwnProperty(entityId)) {
      allChildrenSet(sceneGraphEntities[entityId], 'castShadow', false);
      allChildrenSet(sceneGraphEntities[entityId], 'receiveShadow', false);
    }
  }

  var objects = this.entityManager.getAllComponentsData(['CastShadow']);
  for (var i = 0; i < objects.length; i++) {
    var object = objects[i];

    if (sceneGraphEntities.hasOwnProperty(object.entityId)) {
      allChildrenSet(sceneGraphEntities[object.entityId], 'castShadow', true);
    }
  }

  var objects = this.entityManager.getAllComponentsData(['ReceiveShadow']);
  for (var i = 0; i < objects.length; i++) {
    var object = objects[i];

    if (sceneGraphEntities.hasOwnProperty(object.entityId)) {
      allChildrenSet(sceneGraphEntities[object.entityId], 'receiveShadow', true);
    }
  }

  var objects = this.entityManager.getAllComponentsData(['Material']);
  for (var i = 0; i < objects.length; i++) {
    var object = objects[i];

    if (sceneGraphEntities.hasOwnProperty(object.entityId)) {
      sceneGraphEntities[object.entityId].traverse(child => {
        if (child.hasOwnProperty('material')) {
          child.material.color = object.Material.color;
          child.material.transparent = object.Material.transparent;
          child.material.opacity = object.Material.opacity;
        }
      });
    }
  }
};

/**
 * See components.js
 */
var TransformAnimationSystem = function(entityManager) {
  this.entityManager = entityManager;
};
TransformAnimationSystem.prototype.update = function(dt) {
  var animatableObjects = this.entityManager.getAllComponentsData(['Transform', 'TransformAnimation']);
  for (var i = 0; i < animatableObjects.length; i++) {
    var animatableObject = animatableObjects[i];

    // Add position/scale/rotation time elapsed time
    animatableObject.Transform.position.add(animatableObject.TransformAnimation.position.clone().multiplyScalar(dt));
    animatableObject.Transform.scale.add(animatableObject.TransformAnimation.scale.clone().multiplyScalar(dt));

    animatableObject.Transform.rotation.x += animatableObject.TransformAnimation.rotation.x * dt;
    animatableObject.Transform.rotation.y += animatableObject.TransformAnimation.rotation.y * dt;
    animatableObject.Transform.rotation.z += animatableObject.TransformAnimation.rotation.z * dt;
  }
};

/**
 * See components.js
 */
var TextureAnimationSystem = function(entityManager) {
  this.entityManager = entityManager;
};
TextureAnimationSystem.prototype.update = function(dt) {
  var animatableObjects = this.entityManager.getAllComponentsData(['Transform', 'TextureAnimation']);
  for (var i = 0; i < animatableObjects.length; i++) {
    var animatableObject = animatableObjects[i];

    // Add uv delta by elapsed time

    var sceneGraphEntity = sceneGraphEntities[animatableObject.entityId];
    if (sceneGraphEntity) {
      sceneGraphEntity.material.map.offset.add(animatableObject.TextureAnimation.delta.clone().multiplyScalar(dt));
    }
  }
};

/**
 * See components.js
 */
var RotateWithGlobalWindspeedSystem = function(entityManager) {
  this.entityManager = entityManager;
};
RotateWithGlobalWindspeedSystem.prototype.update = function(dt) {
  var animatableObjects = this.entityManager.getAllComponentsData(['Transform', 'RotateWithGlobalWindspeed', 'RotateWithWindspeed']);
  for (var i = 0; i < animatableObjects.length; i++) {
    var animatableObject = animatableObjects[i];

    if (!globalSettings.liveDataEnabled) {
      animatableObject.RotateWithWindspeed.speed = windspeed;
    }
  }
};

/**
 * See components.js
 */
var RotateWithWindspeedSystem = function(entityManager) {
  this.entityManager = entityManager;
};
RotateWithWindspeedSystem.prototype.update = function(dt) {
  var animatableObjects = this.entityManager.getAllComponentsData(['Transform', 'TransformAnimation', 'RotateWithWindspeed']);
  for (var i = 0; i < animatableObjects.length; i++) {
    var animatableObject = animatableObjects[i];

    // Update rotation animation according to windspeed
    // We compute RPM from windspeed according to characteristic curve, then to Rotations per Millisecond, then radians per millisecond. 
    animatableObject.TransformAnimation.rotation[animatableObject.RotateWithWindspeed.rotationAxis] = windspeedToRPM(animatableObject.RotateWithWindspeed.speed) / 60.0 / 1000.0 * 2.0 * Math.PI;
  }
};

/**
 * See components.js
 */
var RotateWithWindDirectionSystem = function(entityManager) {
  this.entityManager = entityManager;
};
RotateWithWindDirectionSystem.prototype.update = function(dt) {
  var rotatableObjects = this.entityManager.getAllComponentsData(['Transform', 'RotateWithWindDirection']);
  for (var i = 0; i < rotatableObjects.length; i++) {
    var rotatableObject = rotatableObjects[i];

    // Do not update object angle if live data is enabled
    if (!globalSettings.liveDataEnabled) {
      rotatableObject.Transform.rotation[rotatableObject.RotateWithWindDirection.rotationAxis] = windDirection / 180.0 * Math.PI;
    }
  }
};

/**
 * See components.js
 */
var RotateWithCameraDirectionSystem = function(entityManager) {
  this.entityManager = entityManager;
};
RotateWithCameraDirectionSystem.prototype.update = function(dt) {
  var rotatableObjects = this.entityManager.getAllComponentsData(['Transform', 'RotateWithCameraDirection']);
  for (var i = 0; i < rotatableObjects.length; i++) {
    var rotatableObject = rotatableObjects[i];

    rotatableObject.Transform.rotation[rotatableObject.RotateWithCameraDirection.rotationAxis] = Math.PI / 2.0 - camera.rotation.z;
  }
};

/**
 * See components.js
 */
var RelativeToCameraSystem = function(entityManager) {
  this.entityManager = entityManager;
};
RelativeToCameraSystem.prototype.update = function(dt) {
  var entities = this.entityManager.getAllComponentsData(['Transform', 'RelativeToCamera']);
  for (var i = 0; i < entities.length; i++) {
    var entity = entities[i];
    var screenPosition = new THREE.Vector3(entity.RelativeToCamera.position.x, entity.RelativeToCamera.position.y, 0.5);
    entity.Transform.position.copy(screenPosition.unproject(compassCamera));
  }
};

/**
 * See components.js
 */
var MoveTextureWithWindSystem = function(entityManager) {
  this.entityManager = entityManager;
};
MoveTextureWithWindSystem.prototype.update = function(dt) {
  var animatableObjects = this.entityManager.getAllComponentsData(['TextureAnimation', 'MoveTextureWithWind']);
  for (var i = 0; i < animatableObjects.length; i++) {
    var animatableObject = animatableObjects[i];

    animatableObject.TextureAnimation.delta.x = Math.sin(windDirection / 180.0 * Math.PI) * windspeed / 100000.0;
    animatableObject.TextureAnimation.delta.y = -Math.cos(windDirection / 180.0 * Math.PI) * windspeed / 100000.0;
  }
};


/**
 * See components.js
 */
var InitMeshSystem = function(entityManager) {
  this.entityManager = entityManager;
};
InitMeshSystem.prototype.update = function(dt) {
  var objectsWithMesh = this.entityManager.getAllComponentsData(['Mesh']);
  for (var i = 0; i < objectsWithMesh.length; i++) {
    // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
    let objectWithMesh = objectsWithMesh[i];

    if (objectWithMesh.Mesh.needsUpdate) {
      createModelOBJWithMaterial(assetBasePath + objectWithMesh.Mesh.filenameMesh, assetBasePath + objectWithMesh.Mesh.filenameMaterial).then((mesh) => {
        sceneGraphEntities[objectWithMesh.entityId].add(mesh);
      });
      objectWithMesh.Mesh.needsUpdate = false;
    }
  }
};

/**
 * Creates a line from Line.pointA to Line.pointB
 * 
 * Can create different types of line, although only lineType = 'plane' is advised. Threejs lines have to width parameter
 * and are hard to select and the meshLine library is very slow.
 *
 * With 'plane' a slim rectangular plane facing up is rotated so that is connects the point like a line
 */
var LineSystem = function(entityManager) {
  this.entityManager = entityManager;
};
LineSystem.prototype.update = function(dt) {
  var lines = this.entityManager.getAllComponentsData(['Line']);
  for (var i = 0; i < lines.length; i++) {
    // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
    let line = lines[i];
    if (line.Line.needsUpdate) {
      let entitySceneGraphObject = sceneGraphEntities[line.entityId];

      // Remove old connection lines from children
      entitySceneGraphObject.children = entitySceneGraphObject.children.filter(child => !(child.isCable));

      var geometry = new THREE.Geometry();
      geometry.vertices.push(line.Line.pointA.clone() /*.setZ(0.4)*/ );
      geometry.vertices.push(line.Line.pointB.clone() /*.setZ(0.4)*/ );
      var lineType = 'plane';
      if (lineType == 'meshLine') { // Very slow
        // var meshLine = new MeshLine();
        // meshLine.setGeometry(geometry);
        // var material = new MeshLineMaterial({
        //   lineWidth: 0.8,
        //   color: new THREE.Color(0)
        // });
        // var mesh = new THREE.Mesh(meshLine.geometry, material); // this syntax could definitely be improved!
        // mesh.isCable = true;
        // entitySceneGraphObject.add(mesh);
      } else if (lineType == 'line') {
        var material = new THREE.LineBasicMaterial({
          color: 0
        });
        var mesh = new THREE.Line(geometry, material);
        mesh.isCable = true;
        entitySceneGraphObject.add(mesh);
      } else if (lineType == 'plane') {
        var pointA = line.Line.pointA.clone();
        pointA.z = Math.max(0.4, pointA.z);
        var pointB = line.Line.pointB.clone();
        pointB.z = Math.max(0.4, pointA.z);
        var dist = pointA.clone().sub(pointB).length();
        var center = pointA.clone().add(pointB).multiplyScalar(0.5) /*.setZ(0.4)*/ ;
        var planeGeometry = new THREE.PlaneGeometry(dist, 2.5);
        var material = new THREE.MeshBasicMaterial({
          color: 0
        });
        var mesh = new THREE.Mesh(planeGeometry, material);
        mesh.position.copy(center);
        mesh.rotation.z = Math.atan2(pointA.y - pointB.y, pointA.x - pointB.x);
        mesh.rotation.x = Math.atan2(pointA.z - pointB.z, dist);
        if (pointA.z < pointB.z) {
          mesh.rotation.x *= -1;
        }
        // mesh.rotation.x = Math.atan((line.Line.pointB.z - line.Line.pointA.z) / dist);
        mesh.isCable = true;
        entitySceneGraphObject.add(mesh);
      }
      line.Line.needsUpdate = false;
    }
  }
};

/**
 * Sets pointA and pointB of Line to the Transform.position's of the connected entities
 */
var CableConnectionSystem = function(entityManager) {
  this.entityManager = entityManager;
};
CableConnectionSystem.prototype.update = function(dt) {
  var cables = this.entityManager.getAllComponentsData(['Line', 'CableConnection']);
  for (var i = 0; i < cables.length; i++) {
    // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
    let cable = cables[i];
    if (cable.CableConnection.pointA != -1 && cable.CableConnection.pointB != -1) {
      var connectables1 = this.entityManager.getAllComponentsData(['Transform', 'Connectable']).filter(obj => obj.Connectable.id == cable.CableConnection.pointA);
      var connectables2 = this.entityManager.getAllComponentsData(['Transform', 'Connectable']).filter(obj => obj.Connectable.id == cable.CableConnection.pointB);
      if (connectables1.length > 0 && connectables2.length > 0) {
        if (connectables1.length > 1) {
          console.error("Attention!! There are more than one Connectables with id " + cable.CableConnection.pointA);
        }
        if (connectables2.length > 1) {
          console.error("Attention!! There are more than one Connectables with id " + cable.CableConnection.pointB);
        }
        cable.Line.pointA.copy(connectables1[0].Transform.position);
        cable.Line.pointB.copy(connectables2[0].Transform.position);
      } else {
        console.error(`Attention!! Connection wrong: Connectables with id ${cable.CableConnection.pointA}: ${connectables1.length}, with id ${cable.CableConnection.pointB}: ${connectables2.length}`);
      }
    }
  }
};

/**
 * See components.js
 */
var InitPlaneSystem = function(entityManager) {
  this.entityManager = entityManager;
};
InitPlaneSystem.prototype.update = function(dt) {
  var planeObjects = this.entityManager.getAllComponentsData(['Plane']);
  for (var i = 0; i < planeObjects.length; i++) {
    // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
    let planeObject = planeObjects[i];

    if (planeObject.Plane.needsUpdate) {
      var geometry = new THREE.PlaneGeometry(1, 1, 1, 1); // width, height, widthSegments, heightSegments
      var material = new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide
      });
      var mesh = new THREE.Mesh(geometry, material);
      sceneGraphEntities[planeObject.entityId].add(mesh);
      planeObject.Plane.needsUpdate = false;
    }
  }
};

/**
 * See components.js
 */
var InitCircleSystem = function(entityManager) {
  this.entityManager = entityManager;
};
InitCircleSystem.prototype.update = function(dt) {
  var circleObjects = this.entityManager.getAllComponentsData(['Circle']);
  for (var i = 0; i < circleObjects.length; i++) {
    // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
    let circleObject = circleObjects[i];

    if (circleObject.Circle.needsUpdate) {
      var geometry = new THREE.CircleGeometry(1, 16); // width, height, widthSegments, heightSegments
      var material = new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide
      });
      var mesh = new THREE.Mesh(geometry, material);
      sceneGraphEntities[circleObject.entityId].add(mesh);
      circleObject.Circle.needsUpdate = false;
    }
  }
};

/**
 * See components.js
 */
var InitBoxSystem = function(entityManager) {
  this.entityManager = entityManager;
};
InitBoxSystem.prototype.update = function(dt) {
  var boxObjects = this.entityManager.getAllComponentsData(['Box']);
  for (var i = 0; i < boxObjects.length; i++) {
    // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
    let boxObject = boxObjects[i];

    if (boxObject.Box.needsUpdate) {
      var geometry = new THREE.BoxGeometry(1, 1, 1); // width, height, widthSegments, heightSegments
      var material = new THREE.MeshPhongMaterial({
        side: THREE.DoubleSide
      });
      var mesh = new THREE.Mesh(geometry, material);
      sceneGraphEntities[boxObject.entityId].add(mesh);
      boxObject.Box.needsUpdate = false;
    }
  }
};

/**
 * See components.js
 */
var GridPositionByMousePositionSystem = function(entityManager) {
  this.entityManager = entityManager;
};
GridPositionByMousePositionSystem.prototype.update = function(dt) {
  if (mouse.changed && mouse.gridPos != null) {
    var gridPositionedElements = this.entityManager.getAllComponentsData(['Transform', 'GridPosition', 'GridPositionByMousePosition']);
    for (var i = 0; i < gridPositionedElements.length; i++) {
      let gridPositionedElement = gridPositionedElements[i];
      gridPositionedElement.GridPosition.position.copy(mouse.gridPos);

      // If the lockOnHoveredObjects property is set we want to place the gridPositionedElement
      // to the grid position of the object that is hovered (e.g. for the circle that indicates
      // the current grid position under the mouse cursor) 
      if (gridPositionedElement.GridPositionByMousePosition.lockOnHoveredObjects && outlinePass.selectedObjects.length > 0) {
        if (outlinePass.selectedObjects[0].hasComponent('GridPosition')) {
          gridPositionedElement.GridPosition.position.copy(outlinePass.selectedObjects[0].getComponentData('GridPosition').position);
        } else {
          gridPositionedElement.GridPosition.position.set(-100000, -100000, 1000);
        }
      }
    }
  }
};

/**
 * See components.js
 */
var MoveWithCameraSystem = function(entityManager) {
  this.entityManager = entityManager;
};
MoveWithCameraSystem.prototype.update = function(dt) {
  var entities = this.entityManager.getAllComponentsData(['Transform', 'MoveWithCamera']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    entity.Transform.position.copy(controls.target);
  }
};

/**
 * See components.js
 */
var ColorCodeObjectPlaceabilitySystem = function(entityManager) {
  this.entityManager = entityManager;
};
ColorCodeObjectPlaceabilitySystem.prototype.update = function(dt) {
  var entities = this.entityManager.getAllComponentsData(['GridPosition', 'Material', 'ColorCodeObjectPlaceability']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    if (project.isGridPositionRestricted(entity.GridPosition.position)) {
      entity.Material.color = new THREE.Color(1, 0, 0);
    } else {
      entity.Material.color = new THREE.Color(0, 1, 0);
    }
  }
};

/**
 * The average/mid point of two angles (in clockwise order), capable of e.g. averageAngle(-0.1, 0.1)=0.0 and averageAngle(0.1, -0.1)=3.14
 */
function averageAngle(angle1, angle2) {
  if (angle2 < angle1) {
    return ((angle1 + angle2 + Math.PI * 2.0) / 2.0);
  }
  return (angle1 + angle2) / 2.0;
}

/**
 * Difference of two angles (in clockwise order): averageAngle(-0.1, 0.1)=0.2 and averageAngle(0.1, -0.1)=2pi-0.2
 */
function angleDifference(angle1, angle2) {
  if (angle2 < angle1) {
    return angle2 + Math.PI * 2.0 - angle1;
  }
  return angle2 - angle1;
}

/**
 * Places circle segments around the windmill foot
 *
 * WindDistribution.data == [(angle1, speed), (angle2, speed), (angle3, speed)] places three
 * segments, with the first (centered at angle1) from averageAngle(angle3, angle1) to averageAngle(angle1, angle2)
 * with radius speed (+3)
 *
 * Results in a nice circular wind distribution
 */
var WindDistributionSystem = function(entityManager) {
  this.entityManager = entityManager;
};
WindDistributionSystem.prototype.update = function(dt) {
  var entities = this.entityManager.getAllComponentsData(['WindDistribution']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    var sceneGraphEntity = sceneGraphEntities[entity.entityId];
    sceneGraphEntity.visible = globalSettings.windDistributionVisible;
    if (entity.WindDistribution.needsUpdate) {
      // Delete previous circle objects
      sceneGraphEntity.children = sceneGraphEntity.children.filter(child => !child.isWindDistribution);

      if (entity.WindDistribution.data.length >= 3) {
        var geometry = new THREE.Geometry();
        // var geometry = new THREE.CircleGeometry(100.0, entity.WindDistribution.data.length * 2);

        for (var i = 0; i < entity.WindDistribution.data.length; i++) {
          // var vertex1 = geometry.vertices[geometry.vertices.length - 1 - 2 * i];
          // var vertex2 = geometry.vertices[geometry.vertices.length - 1 - 2 * i - 1];
          var dataPointBefore = entity.WindDistribution.data[i];
          var dataPoint = entity.WindDistribution.data[(i + 1) % entity.WindDistribution.data.length];
          var dataPointAfter = entity.WindDistribution.data[(i + 2) % entity.WindDistribution.data.length];

          var angle1 = averageAngle(dataPointBefore.angle, dataPoint.angle);
          var angle2 = averageAngle(dataPoint.angle, dataPointAfter.angle);
          // console.log(angle1);
          // console.log(angle2);
          // console.log(angleDifference(angle1, angle2));
          var length = 3.0 + dataPoint.speed;
          // vertex1.x = Math.sin(angle1) * length;
          // vertex1.y = Math.cos(angle1) * length;
          // vertex2.x = Math.sin(angle2) * length;
          // vertex2.y = Math.cos(angle2) * length;
          var arcGeometry = new THREE.CircleGeometry(length, 10, angle1, angleDifference(angle1, angle2));
          geometry.merge(arcGeometry);
        }

        var mesh = new THREE.Mesh(
          geometry,
          new THREE.MeshBasicMaterial({
            color: 0xffff00,
            side: THREE.BackSide
          })
        );
        mesh.isWindDistribution = true;

        var wingGroupEntity = createEntity(['Transform'], mesh, {
          'Transform': {
            position: new THREE.Vector3(0, 0, 0.5),
            rotation: new THREE.Euler(Math.PI, 0, -Math.PI / 2.0)
          }
        }).addToObject(sceneGraphEntity);
      }
      entity.WindDistribution.needsUpdate = false;
    }
  }
};

/**
 * Updates the UI. Is called as the last system in a render cycle.
 *
 * If an entity is selected, the entity specific information (Windmill/Powerstation/Cable) is shown, as well as the
 * entity id, the live id, position/grid position data etc. in a table consisting of property/value pairs.
 *
 * In the global information box its shown whether live data is activated, what the global wind speed is as well as
 * the total cable length.
 */
var UIUpdateSystem = function(entityManager) {
  this.entityManager = entityManager;
};
UIUpdateSystem.prototype.update = function(dt) {
  if (mouse.changed && mouse.gridPos != null) {
    var selectedEntities = this.entityManager.getAllComponentsData(['Selectable']).filter(el => el.Selectable.selected);
    if (selectedEntities.length > 0) {
      var selectedEntity = selectedEntities[0];

      var properties = [];
      var title = '';

      if (entityHasComponent(selectedEntity, 'Windmill')) {
        title = `Windmill`;
        var windmillData = getComponentDataForEntity('Windmill', selectedEntity);
        properties.push({
          name: 'Efficiency',
          value: `${(windmillData.efficiency*100.0).toFixed(2)} %`
        });
      } else if (entityHasComponent(selectedEntity, 'Powerstation')) {
        title = `Powerstation`;
        var powerstationData = getComponentDataForEntity('Powerstation', selectedEntity);
        properties.push({
          name: 'Efficiency',
          value: `${(powerstationData.efficiency*100.0).toFixed(2)} %`
        });
      }

      properties.push({
        name: 'entityId',
        value: selectedEntity.entityId
      });

      if (entityHasComponent(selectedEntity, 'Connectable')) {
        var selectableId = getComponentDataForEntity('Connectable', selectedEntity).id;
        properties.push({
          name: 'ConnectableId',
          value: selectableId
        });
      }
      if (entityHasComponent(selectedEntity, 'GridPosition')) {
        var position = getComponentDataForEntity('GridPosition', selectedEntity);
        properties.push({
          name: 'Position',
          value: `( ${position.position.x} | ${position.position.y} )`
        });
      } else if (entityHasComponent(selectedEntity, 'CableConnection')) {
        title = `Cable`;
        var cableData = getComponentDataForEntity('CableConnection', selectedEntity);
        properties.push({
          name: 'Connection',
          value: `${cableData.pointA} <--> ${cableData.pointB}`
        });
        var lineData = getComponentDataForEntity('Line', selectedEntity);
        properties.push({
          name: 'Cable Length',
          value: `${lineData.pointA.distanceTo(lineData.pointB).toFixed(2)} m`
        });
      } else {
        var transform = getComponentDataForEntity('Transform', selectedEntity);
        properties.push({
          name: 'Position',
          value: `( ${transform.position.x} | ${transform.position.y} | ${transform.position.z} )`
        });
      }

      if (entityHasComponent(selectedEntity, 'LiveData')) {
        var liveDataId = getComponentDataForEntity('LiveData', selectedEntity).id;
        properties.push({
          name: 'Live data ID',
          value: liveDataId
        });
      }

      visualizationDetailBox.updateDetailBox({
        title: title,
        properties: properties,
        entityId: selectedEntity.entityId
      });

    } else {
      visualizationDetailBox.updateDetailBox(null);
    }
  }

  if (globalSettings.globalInfoWindowVisible) {
    var cableLength = this.entityManager.getAllComponentsData(['Line', 'CableConnection']).reduce((acc, cable) => acc + cable.Line.pointA.distanceTo(cable.Line.pointB), 0);

    // Global info
    visualizationGlobalDetailBox.updateDetailBox({
      title: 'Global Information',
      properties: [{
        name: 'Live',
        value: globalSettings.liveDataEnabled ? 'ON' : 'OFF'
      }, {
        name: 'Wind Speed',
        value: `${windspeed.toFixed(2)} m/s`
      }, {
        name: 'Total Cable Length',
        value: `${cableLength.toFixed(2)} m`
      }]
    });
  } else {
    visualizationGlobalDetailBox.updateDetailBox(null);
  }
};

// Add the systems to the entity manager in the order they should be executed, e.g. GridPositionSystem
// before SceneGraphUpdateSystem etc.

entityManager.addProcessor(new SettingsSystem(entityManager));

entityManager.addProcessor(new InitMeshSystem(entityManager));
entityManager.addProcessor(new InitPlaneSystem(entityManager));
entityManager.addProcessor(new InitCircleSystem(entityManager));
entityManager.addProcessor(new InitBoxSystem(entityManager));

entityManager.addProcessor(new RotateWithGlobalWindspeedSystem(entityManager));
entityManager.addProcessor(new RotateWithWindspeedSystem(entityManager));
entityManager.addProcessor(new TransformAnimationSystem(entityManager));
entityManager.addProcessor(new TextureAnimationSystem(entityManager));
entityManager.addProcessor(new MoveWithCameraSystem(entityManager));
entityManager.addProcessor(new GridPositionByMousePositionSystem(entityManager));
entityManager.addProcessor(new GridPositionSystem(entityManager));
entityManager.addProcessor(new ExtendToGroundSystem(entityManager));
entityManager.addProcessor(new CableConnectionSystem(entityManager));
entityManager.addProcessor(new LineSystem(entityManager));
entityManager.addProcessor(new RotateWithWindDirectionSystem(entityManager));
entityManager.addProcessor(new RotateWithCameraDirectionSystem(entityManager));
entityManager.addProcessor(new RelativeToCameraSystem(entityManager));
entityManager.addProcessor(new MoveTextureWithWindSystem(entityManager));
entityManager.addProcessor(new ColorCodeObjectPlaceabilitySystem(entityManager));
entityManager.addProcessor(new WindDistributionSystem(entityManager));

entityManager.addProcessor(new SceneGraphUpdateSystem(entityManager));
entityManager.addProcessor(new UIUpdateSystem(entityManager));
