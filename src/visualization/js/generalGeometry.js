// Init obj, material and texture loaders with the correct asset paths
var loaderOBJ = new THREE.OBJLoader();
var mtlLoader = new THREE.MTLLoader();
mtlLoader.setTexturePath('/src/visualization/models/');
var textureLoader = new THREE.TextureLoader().setPath('/src/visualization/');

/**
 * Loads a texture
 * 
 * @return promise of the texture object
 */
function loadTexture(textureURL) {
  return new Promise((resolve, reject) => {
    textureLoader.load(textureURL, texture => {
        resolve(texture);
      }, null,
      error => reject(error));
  });
}

/**
 * Loads multiple textures
 * 
 * @return promise of an array of the texture objects
 */
function loadTextures(textureURLs) {
  return new Promise((resolve, reject) => {
    var promises = [];
    for (var i = 0; i < 6; i++) {
      promises.push(loadTexture(textureURLs[i]));
    }
    Promise.all(promises).catch(reject).then(resolve);
  });
}

/**
 * Create a cube with position, scale and color
 * 
 * @return promise of the Object3D
 */
function createCube(position, scale, color) {
  var geometry = new THREE.BoxGeometry(scale.x, scale.y, scale.z);
  var material = new THREE.MeshPhongMaterial({
    color: color
  });
  var mesh = new THREE.Mesh(geometry, material);
  mesh.position.copy(position);
  mesh.receiveShadow = true;

  return Promise.resolve(mesh);
}

/**
 * Creates a mesh from an obj file with its material
 *
 * @return promise of the mesh
 */
function createModelOBJWithMaterial(filenameMesh, filenameMaterial) {
  return new Promise((resolve, reject) => {
    mtlLoader.load(filenameMaterial, function(materials) {
      materials.preload();
      var loaderOBJ = new THREE.OBJLoader();
      loaderOBJ.setMaterials(materials);
      loaderOBJ.load(filenameMesh, function(mesh) {
        resolve(mesh);
      });
    });
  });
}