// Map loading bookmarks: 
// Tile calculations http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
// Map box terrain blog entry, conversion rgb to height https://blog.mapbox.com/global-elevation-data-6689f1d0ba65
// Mapbox url specification https://www.mapbox.com/api-documentation/#retrieve-tiles
// Mapbox example webpage for terrain loaded into threejs https://blog.mapbox.com/bringing-3d-terrain-to-the-browser-with-three-js-410068138357, https://www.mapbox.com/bites/00332/#14.5666/36.3241/-112.7478/18.0142/30.5289

// Global objects for ground
var ground;

/**
 * Transfers val from range [minSrc, maxSrc] to [minDest, maxDest]
 */
function transferRange(minSrc, maxSrc, val, minDest, maxDest) {
  return minDest + ((val - minSrc) / (maxSrc - minSrc)) * (maxDest - minDest);
}

// var heightmapSampleData = [
//   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//   [-1, -1, -2, -1, -2, -1, -2, -1, -1, -1, -1, -2, -1, -2, -1, -1, -1, -1, -1, 1, 2, 1, -2, -1, -2, -1, -1, -1, -1, -2, -1, -2, -1, -2, -1, -1],
//   [-2, -2, -3, -2, -3, -2, -3, -2, -2, -2, -2, -3, -2, -3, -1, -1, -2, -2, -2, 2, 3, 2, -3, -2, -3, -2, -2, -2, -2, -3, -2, -3, -2, -3, -2, -2],
//   [-3, -3, -4, -3, -4, -3, -4, -3, -3, -3, -3, -4, -3, -4, -1, -1, -3, -3, -3, 3, 4, -3, -4, -3, -4, -3, -3, -3, -3, -4, -3, -4, -3, -4, -3, -3],
//   [-4, -4, -5, -4, -5, -4, -5, -4, -4, -4, -4, -5, -4, -5, -1, -1, -4, -4, -4, 4, 5, -4, -5, -4, -5, -4, -4, -4, -4, -5, -4, -5, -4, -5, -4, -4],
//   [-5, -5, -6, -5, -6, -5, -6, -5, -5, -5, -5, -6, -5, -6, -1, -1, -5, -5, -5, 5, 6, -5, -6, -5, -6, -5, -5, -5, -5, -6, -5, -6, -5, -6, -5, -5],
//   [-6, -6, -7, -6, -7, -6, -7, -6, -6, -6, -6, -7, -6, -7, -1, -1, -6, -6, -6, 6, 7, -6, -7, -6, -7, -6, -6, -6, -6, -7, -6, -7, -6, -7, -6, -6],
//   [-7, -7, -8, -7, -8, -7, -8, -7, -7, -7, -7, -8, -7, -8, -1, -1, -7, -7, -7, -7, -8, -7, -8, -7, -8, -7, -7, -7, -7, -8, -7, -8, -7, -8, -7, -7],
//   [-8, -8, -9, -8, -9, -8, -9, -8, -8, -8, -8, -9, -8, -9, -8, -9, -8, -8, -8, -8, -9, -8, -9, -8, -9, -8, -8, -8, -8, -9, -8, -9, -8, -9, -8, -8],
//   [-9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9],
//   [-9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9],
//   [-9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9, -9, -9, -10, -9, -10, -9, -10, -9, -9]
// ];

/**
 * Heightmap class, stores a two-dimensional array of heights, the pixelWidth (pixel as the entries come from a heightmap image) which is the world space distance between two adjacent heights,
 * as well as the grid spacing, the distance between two adjacent grid points. 
 */
class Heightmap {
  constructor(heightData, pixelWidth, gridSpacing) {
    this.heightData = heightData;
    this.pixelWidth_ = pixelWidth;
    this.gridSpacing_ = gridSpacing;
    this.outOfBoundsDepth = -50;

    this.size_ = {
      x: this.heightData[0].length,
      y: this.heightData.length
    };
    this.gridSize_ = {
      x: Math.ceil(this.size_.x * this.pixelWidth / this.gridSpacing_) + 1,
      y: Math.ceil(this.size_.y * this.pixelWidth / this.gridSpacing_) + 1
    };
  }

  /**
   * Grid size is the number of grid points on this heightmap in each direction
   */
  get gridSize() {
    return this.gridSize_;
  }

  /**
   * Size is the width and height of the heightmap
   */
  get size() {
    return this.size_;
  }

  get center() {
    return new THREE.Vector2(Math.floor(this.gridSize_.x / 2), Math.floor(this.gridSize_.y / 2));
  }

  get gridSpacing() {
    return this.gridSpacing_;
  }

  get pixelWidth() {
    return this.pixelWidth_;
  }

  get upperLeftCorner() {
    return {
      x: 0,
      y: 0
    };
  }

  get lowerRightCorner() {
    return {
      x: this.size_.x * this.pixelWidth_,
      y: -this.size_.y * this.pixelWidth_
    };
  }

  /**
   * Allows to use the methods both like getHeight(0,0) and like getHeight(new THREE.Vector2(0,0))
   */
  static getPosVecFromParams(param1, param2) {
    // Use either param1 as vector or both params as coordinates
    if (param1.constructor.name == 'Vector2') {
      return param1;
    } else {
      return new THREE.Vector2(param1, param2);
    }
  }

  /**
   * Returns the heightmap height at heightmap position pos
   */
  getHeight(param1, param2) {
    var pos = Heightmap.getPosVecFromParams(param1, param2);
    if (pos.x >= 0 && pos.x < this.size_.x && pos.y >= 0 && pos.y < this.size_.y) {
      return this.heightData[pos.y][pos.x];
    } else {
      return this.outOfBoundsDepth;
    }
  }

  /**
   * Converts grid coordinates to heightmap/pixel coordinates
   */
  gridToPixelPos(param1, param2) {
    var gridPosition = Heightmap.getPosVecFromParams(param1, param2);
    let x = Math.round(gridPosition.x * this.gridSpacing_ / this.pixelWidth_);
    let y = Math.round(gridPosition.y * this.gridSpacing_ / this.pixelWidth_);

    // The last grid positions might be rounded to a pixel coordinate outside the heightmap, so in that case use
    // the last valid value (keeps objects from falling to 0 right at the edge)
    if (x == this.size_.x) {
      x -= 1;
    }
    if (y == this.size_.y) {
      y -= 1;
    }
    return new THREE.Vector2(x, y);
  }

  /**
   * Get height nearest to a grid position
   */
  getGridHeight(param1, param2) {
    return this.getHeight(this.gridToPixelPos(param1, param2));
  }

  /**
   * Get the minimal grid value in a 3x3 neighborhood around gridPosition, the windmill foot will extend to this minimal height
   */
  getGridMinInNeighborhood(gridPosition) {
    var heightmapPos = this.gridToPixelPos(gridPosition);
    var minVal = this.getHeight(heightmapPos);
    for (var yi = Math.max(heightmapPos.y - 1, 0); yi <= Math.min(heightmapPos.y + 1, this.size.y - 1); yi++) {
      for (var xi = Math.max(heightmapPos.x - 1, 0); xi <= Math.min(heightmapPos.x + 1, this.size.x - 1); xi++) {
        minVal = Math.min(minVal, this.getHeight(xi, yi));
      }
    }
    return minVal;
  }

  /**
   * Get the heightmap height, but return 0.1 for values 0<x<0.1 and -0.1 for -0.1<x<0 to prevent z-fighting at coasts
   */
  getHeightRendering(param1, param2) {
    var heightmapHeight = this.getHeight(param1, param2);
    if (heightmapHeight >= 0) {
      return Math.max(0.1, heightmapHeight);
    }
    return Math.min(-0.1, heightmapHeight);
  }

  /**
   * converts grid coordinates to world coordinates
   */
  gridCoordinatesToWorldCoordinates(gridPosition) {
    let x = gridPosition.x * this.gridSpacing;
    let y = -gridPosition.y * this.gridSpacing;
    return new THREE.Vector3(x, y, this.getGridHeight(gridPosition));
  }

  /**
   * converts world coordinates to grid coordinates
   */
  worldCoordinatesToGridCoordinates(worldPosition) {
    let x = Math.round(worldPosition.x / this.gridSpacing);
    let y = Math.round(-worldPosition.y / this.gridSpacing);
    return new THREE.Vector2(x, y);
  }

  /** 
   * Converts world coordinates to latitude/longitude
   */
  worldCoordinatesToLatLong(worldPosition, bounds) {
    var heightmapUpperLeftCorner = this.upperLeftCorner;
    var heightmapLowerRightCorner = this.lowerRightCorner;
    var long = transferRange(heightmapUpperLeftCorner.x, heightmapLowerRightCorner.x, worldPosition.x, bounds.latLongBounds.min.long, bounds.latLongBounds.max.long);
    var lat = transferRange(heightmapUpperLeftCorner.y, heightmapLowerRightCorner.y, worldPosition.y, bounds.latLongBounds.min.lat, bounds.latLongBounds.max.lat);
    // var long = project.terrain.bounds.latLongBounds.min.long + meterToLatlongCoord(-heightmapUpperLeftCorner.x + worldPosition.x);
    // var lat = project.terrain.bounds.latLongBounds.min.lat - meterToLatlongCoord(heightmapUpperLeftCorner.y - worldPosition.y);
    return {
      lat: lat,
      long: long
    };
  }

  /** 
   * Converts latitude/longitude to world coordinates
   */
  latLongToWorldCoordinates(coords, bounds) {
    var heightmapUpperLeftCorner = this.upperLeftCorner;
    var heightmapLowerRightCorner = this.lowerRightCorner;
    var x = transferRange(bounds.latLongBounds.min.long, bounds.latLongBounds.max.long, coords.long, heightmapUpperLeftCorner.x, heightmapLowerRightCorner.x);
    var y = transferRange(bounds.latLongBounds.min.lat, bounds.latLongBounds.max.lat, coords.lat, heightmapUpperLeftCorner.y, heightmapLowerRightCorner.y);
    return new THREE.Vector3(x, y, 0);
  }
}

/**
 * Abstract class for terrain bounds
 */
class Bounds {
  constructor(bounds) {
    this.min = this.computeBounds(bounds.min);
    this.max = this.computeBounds(bounds.max);
    this.latLongBounds = {
      min: this.min,
      max: this.max
    };
  }

  computeBounds(coord) {
    return {
      lat: coord.lat,
      long: coord.long
    };
  }

  get numTiles() {
    return {
      x: 1,
      y: 1
    };
  }
}

/**
 * Terrain bounds for mapbox terrains, holds information about zoom level, resolution and computes tile
 * bounds (see http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
 */
class TileBounds extends Bounds {
  constructor(bounds, tileOptions) {
    super(bounds);
    this.zoom = tileOptions.zoom;
    this.resolution = tileOptions.resolution;
    this.computeTileBounds(this.min, bounds.min, tileOptions.zoom);
    this.computeTileBounds(this.max, bounds.max, tileOptions.zoom);

    // Taken from http://www.maptiler.org/google-maps-coordinates-tile-bounds-projection/
    this.originShift = 2 * Math.PI * 6378137 / 2.0;
    this.initialResolution = 2 * Math.PI * 6378137 / tileOptions.resolution /*project.terrain.tileOptions*/ ;
    this.latLongBounds = this.latLongBounds_();
  }

  /**
   * Computes the x/y tile coordinates at coord with zoom level zoom and stores it in obj
   */
  computeTileBounds(obj, coord, zoom) {
    obj.x = this.long2tile(coord.long, zoom);
    obj.y = this.lat2tile(coord.lat, zoom);
  }

  // Conversion functions from http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
  long2tile(lon, zoom) {
    return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
  }
  lat2tile(lat, zoom) {
    return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
  }

  get numTiles() {
    return {
      x: this.max.x - this.min.x + 1,
      y: this.max.y - this.min.y + 1
    };
  }

  // Taken from http://www.maptiler.org/google-maps-coordinates-tile-bounds-projection/
  res(zoom) {
    return this.initialResolution / (Math.pow(2, this.zoom));
  }

  /**
   * Computes the world space edge lengths of a pixel
   */
  pixelsToMeters(px, py) {
    var res = this.res(this.zoom);
    var mx = px * res - this.originShift;
    var my = py * res - this.originShift;
    return {
      x: mx,
      y: my
    };
  }

  tileBounds(tx, ty) {
    var min = this.pixelsToMeters(tx * this.resolution, ty * this.resolution, this.zoom);
    var max = this.pixelsToMeters((tx + 1) * this.resolution, (ty + 1) * this.resolution, this.zoom);
    return {
      min: min,
      max: max
    };
  }

  metersToLatLon(mx, my) {
    var long = (mx / this.originShift) * 180.0;
    var lat = (my / this.originShift) * 180.0;

    lat = -180.0 / Math.PI * (2 * Math.atan(Math.exp(lat * Math.PI / 180.0)) - Math.PI / 2.0);
    return {
      lat: lat,
      long: long
    };
  }

  /**
   * Computes the lat/long bounds of the terrain
   */
  tileLatLongBounds(tx, ty) {
    var bounds = this.tileBounds(tx, ty);
    var min = this.metersToLatLon(bounds.min.x, bounds.min.y);
    var max = this.metersToLatLon(bounds.max.x, bounds.max.y);
    return {
      min: min,
      max: max
    };
  }

  /**
   * Computes the lat/long bounds of the terrain
   */
  latLongBounds_() {
    return {
      min: this.tileLatLongBounds(this.min.x, this.min.y).min,
      max: this.tileLatLongBounds(this.max.x, this.max.y).max
    };
  }
}

/**
 * Abstract class for a restricted area
 */
class RestrictedArea {
  coordInside(coord) {
    console.warn('coordInside not overridden');
  }
}

/**
 * Restricted area that forms a circle
 */
class RestrictedAreaCircle extends RestrictedArea {
  constructor(center, radius, insideout) {
    super();
    this.center = center;
    this.radius = radius;
    this.insideout = insideout;
  }

  coordInside(coord) {
    var worldPosition = project.terrain.heightmap.latLongToWorldCoordinates(coord, project.terrain.bounds);
    var centerWorldPosition = project.terrain.heightmap.latLongToWorldCoordinates(this.center, project.terrain.bounds);

    // Return true if coord is inside circle and this.insideout==false or coord is outside and this.insideout==true
    return (worldPosition.distanceTo(centerWorldPosition) < this.radius) != this.insideout;
  }

  // Computes the euclidian distance of coord to this.center (good approximation for small distances)
  // http://jonisalonen.com/2014/computing-distance-between-coordinates-can-be-simple-and-fast/
  coordDistance(coord) {
    var deglen = 110.25 * 1000.0;
    var x = coord.lat - this.center.lat;
    var y = (coord.long - this.center.long) * Math.cos(this.center.lat);
    return deglen * Math.sqrt(x * x + y * y);
  }

  getJSON() {
    return {
      type: 'circle',
      center: this.center,
      radius: this.radius
    };
  }
}

/**
 * Restricted area in form of a polygon
 */
class Polygon extends RestrictedArea {
  constructor(points, insideout) {
    super();
    this.points = points;
    this.triangulation = THREE.ShapeUtils.triangulate(points);
    this.insideout = insideout;
  }

  pointInside(point) {
    for (var i = 0; i < this.triangulation.length; i++) {
      if (Polygon.pointInTriangle(point, this.triangulation[i])) {
        // Return true for polygon where inside is restricted, false if outside is restricted
        return !this.insideout;
      }
    }
    return this.insideout;
  }

  /**
   * For use with coordinate object
   */
  coordInside(coord) {
    return this.pointInside({
      x: -coord.lat,
      y: coord.long,
    });
  }

  static pointInTriangle(point, triangle) {
    return Polygon.sign(point, triangle[0], triangle[1]) >= 0 && Polygon.sign(point, triangle[1], triangle[2]) >= 0 && Polygon.sign(point, triangle[2], triangle[0]) >= 0;
  }

  static sign(p1, p2, p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
  }

  getJSON() {
    return {
      type: 'polygon',
      coords: this.points.map(point => {
        return {
          lat: -point.x,
          long: point.y
        };
      })
    };
  }
}

/**
 * Restricted area polygon from a rectangular, not used at the moment
 */
class RectanglePolygon extends Polygon {
  constructor(bounds, insideout) {
    var points = [];
    points.push(new THREE.Vector2(-bounds.max.lat, bounds.min.long));
    points.push(new THREE.Vector2(-bounds.max.lat, bounds.max.long));
    points.push(new THREE.Vector2(-bounds.min.lat, bounds.max.long));
    points.push(new THREE.Vector2(-bounds.min.lat, bounds.min.long));
    super(points, insideout);
  }
}

/**
 * Subclass for creating the polygon with coordinate corner points
 */
class CoordPolygon extends Polygon {
  constructor(coords, insideout) {
    super(coords.map(coord => new THREE.Vector2(-coord.lat, coord.long)), insideout);
  }
}

// function getPlaneColorDependingOnDepth(depthValue) {
//   var clippedDepthValue = Math.max(-10, Math.min(depthValue, 10));
//   var color1, color2, ratio;
//   if (clippedDepthValue < 0) {
//     color1 = 'e6e6ff';
//     color2 = '000033';
//     ratio = 1 - (-1 * clippedDepthValue / 10.0);
//   } else {
//     color1 = 'ccff33';
//     color2 = 'cc3300';
//     ratio = 1 - (clippedDepthValue / 10.0);
//   }
//   var hex = function(x) {
//     x = x.toString(16);
//     return (x.length == 1) ? '0' + x : x;
//   };

//   var r = Math.ceil(parseInt(color1.substring(0, 2), 16) * ratio + parseInt(color2.substring(0, 2), 16) * (1 - ratio));
//   var g = Math.ceil(parseInt(color1.substring(2, 4), 16) * ratio + parseInt(color2.substring(2, 4), 16) * (1 - ratio));
//   var b = Math.ceil(parseInt(color1.substring(4, 6), 16) * ratio + parseInt(color2.substring(4, 6), 16) * (1 - ratio));

//   return "rgb(" + r + ", " + g + ", " + b + ")";
// }

/**
 * Abstract terrain class that holds the heightmap, textures, terrain bounds, restricted areas
 */
class Terrain {
  constructor(terrainOptions) {
    this.gridSpacing = terrainOptions.gridSpacing;
    this.heightSubtractor = terrainOptions.heightSubtractor || 0;
    this.heightFactor = terrainOptions.heightFactor || 1;
    this.pixelWidth = terrainOptions.pixelWidth || 1;
    this.fullyLoaded = false;

    this.gridHighlightElement = null;

    // Create empty heightmap first. This is needed as heightmap is possibly loaded asynchronously
    this.heightmap = new Heightmap([
      []
    ], 1, 1);
    this.textures = [];

    // Create the initial project restricted area from terrainOptions
    if (terrainOptions.bounds) {
      this.bounds = new Bounds(terrainOptions.bounds);
      this.restrictedAreas = [new RectanglePolygon(this.bounds, true)];
    } else {
      this.bounds = new Bounds(Terrain.boundsFromPolygon(terrainOptions.boundingPolygon));
      this.restrictedAreas = [new CoordPolygon(terrainOptions.boundingPolygon, true)];
    }

    // Create other restricted areas from terrainOptions.restrictedAreas
    if (terrainOptions.restrictedAreas && terrainOptions.restrictedAreas.length > 0) {
      this.restrictedAreas = this.restrictedAreas.concat(
        terrainOptions.restrictedAreas
        .filter(restrictedArea => restrictedArea.type == 'polygon')
        .map(polygon => new CoordPolygon(polygon.coords, false))
      );

      this.restrictedAreas = this.restrictedAreas.concat(
        terrainOptions.restrictedAreas
        .filter(restrictedArea => restrictedArea.type == 'circle')
        .map(circle => new RestrictedAreaCircle(circle.center, circle.radius, false))
      );
    }
  }

  /** 
   * For saving the restricted areas
   */
  getRestrictedAreas() {
    return this.restrictedAreas.slice(1).map(restrictedArea => restrictedArea.getJSON());
  }

  /**
   * Create min/max bounds from a polygon (upper left/lower right coordinate of smallest axis-oriented
   * rectangle that covers the polygon)
   */
  static boundsFromPolygon(boundingPolygon) {
    return {
      min: boundingPolygon.reduce((currentAccumulator, coords) => {
        return {
          lat: Math.max(currentAccumulator.lat, coords.lat),
          long: Math.min(currentAccumulator.long, coords.long)
        };
      }),
      max: boundingPolygon.reduce((currentAccumulator, coords) => {
        return {
          lat: Math.min(currentAccumulator.lat, coords.lat),
          long: Math.max(currentAccumulator.long, coords.long)
        };
      })
    };
  }

  /**
   * Destroys the terrain
   */
  destroy() {
    if (ground) {
      ground.destroy();
    }
    if (this.gridHighlightElement) {
      this.gridHighlightElement.destroy();
    }
  }

  /**
   * Creates a restricted area from a grid position polygon
   */
  addRestrictedPolygonFromGridPositions(polygon) {
    var pointsInCoords = polygon.map(point => this.heightmap.worldCoordinatesToLatLong(this.heightmap.gridCoordinatesToWorldCoordinates(point), this.bounds));
    this.restrictedAreas.push(new CoordPolygon(pointsInCoords, false));
    this.colorGroundTiles();
  }

  /**
   * Creates a restricted area from a circle on the grid
   */
  addRestrictedCircleFromGridPosition(gridPos, radius) {
    var coord = this.heightmap.worldCoordinatesToLatLong(this.heightmap.gridCoordinatesToWorldCoordinates(gridPos), this.bounds);
    this.restrictedAreas.push(new RestrictedAreaCircle(coord, radius, false));
    this.colorGroundTiles();
  }

  /**
   * Remove all restricted areas that cover gridPosition
   */
  removeRestrictedAreasAtGridPosition(gridPosition) {
    var coords = this.heightmap.worldCoordinatesToLatLong(this.heightmap.gridCoordinatesToWorldCoordinates(gridPosition), this.bounds);
    this.restrictedAreas = this.restrictedAreas.filter((polygon, index) => index == 0 || !polygon.coordInside(coords));
    this.colorGroundTiles();
  }

  /**
   * Checks whether coord is restricted by any restricted area
   */
  isCoordRestricted(coord) {
    for (var i = 0; i < this.restrictedAreas.length; i++) {
      if (this.restrictedAreas[i].coordInside(coord)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether point p (in world coordinates) is restricted by any restricted area or by hardcoded height constraints
   */
  isPointRestricted(p) {
    return p.z > 450.0 || p.z < -100.0 || this.isCoordRestricted(this.heightmap.worldCoordinatesToLatLong(p, this.bounds));
  }

  /**
   * Checks whether the terrain has any textures
   */
  hasTextures() {
    return this.textures && this.textures.length > 0;
  }

  /**
   * Checks whether the terrain has all its textures
   */
  hasAllTextures() {
    return this.textures && this.textures.length == this.bounds.numTiles.x * this.bounds.numTiles.y;
  }

  /**
   * Creates the heightmap from heightArray and invokes the creation of the ground meshes
   */
  createTerrainGeometry(heightArray, numTilesX, numTilesY) {
    this.heightmap = new Heightmap(heightArray, this.pixelWidth, this.gridSpacing);
    window.heightmap = this.heightmap;

    this.destroy();

    this.createGroundFromHeightmap();
  }

  /**
   * Parses an arraybuffer as PNG image
   */
  parsePNGBuffer(arrayBuffer) {
    return new Promise((resolve, reject) => {
      (new png.PNG(arrayBuffer)).parse(arrayBuffer, function(error, data) {
        if (error) reject(error);
        else resolve(data);
      });
    });
  }

  /**
   * Downloads a binary file, returns a promise
   */
  requestBinary(url) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url, true);
      xhr.responseType = "arraybuffer";
      xhr.onload = function(e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(xhr.response);
          } else {
            reject(xhr.statusText);
          }
        }
      };
      xhr.onerror = function(e) {
        console.error(xhr.statusText);
      };
      xhr.send(null);
    });
  }

  /**
   * Parses image pixels of an image im (can be a jimp or pngjs image object) and writes the computed height values
   * to the heightmap, loads and returns the texture if present. 
   */
  parseImagePixels(im, heightmapDataObject, tileOffsetX, tileOffsetY, convertMapbox, resolution, texture) {
    var isJimpObject = im.getPixelColor;
    var imwidth = isJimpObject ? im.bitmap.width : im.width;
    var imheight = isJimpObject ? im.bitmap.height : im.height;

    for (var xi = 0; xi < imwidth; xi += imwidth / resolution) {
      for (var yi = 0; yi < imheight; yi += imheight / resolution) {
        var height;
        var c;
        if (isJimpObject) {
          c = Jimp.intToRGBA(im.getPixelColor(xi, yi));
        } else {
          var idx = (imwidth * yi + xi) << 2;
          c = {
            r: im.data[idx],
            g: im.data[idx + 1],
            b: im.data[idx + 2],
            a: im.data[idx + 3]
          };
        }
        if (convertMapbox) {
          // if (xi == 0 && yi == 0) console.log(c);
          height = -10000 + ((c.r * 256 * 256 + c.g * 256 + c.b) * 0.1) + this.heightSubtractor;
          if (height <= 0.0) {
            // The height below the ocean is for illustration purposes always lower than -10, as mapbox does not give a lot of ocean
            // depth information
            height = Math.min(height, -10.0);
          }
        } else {
          height = c.r / 255.0 * this.heightFactor + this.heightSubtractor;
        }
        heightmapDataObject.heightmapData[tileOffsetY * resolution + yi / imwidth * resolution][tileOffsetX * resolution + xi / imwidth * resolution] = height;
      }
    }
    var TextureMap = null;
    if (texture) {
      TextureMap = new THREE.TextureLoader().load(texture);
    }
    return TextureMap;
  }

  load(dontResetCamera) {
    console.error("Abstract Terrain.load not implemented (must be overridden)");
  }

  /**
   * Colors the plane according to height information.
   * 
   * Commented out (like getPlaneColorDependingOnDepth). Is this working correctly?
   */
  colorGroundTileHeight(planeGeometry, offsetX, offsetY) {
    //   var numberOfFaces = planeGeometry.faces.length;
    //   for (var i = 0; i < numberOfFaces; i++) {
    //     var firstColor = new THREE.Color(getPlaneColorDependingOnDepth(planeGeometry.vertices[planeGeometry.faces[i].a].z));
    //     var secondColor = new THREE.Color(getPlaneColorDependingOnDepth(planeGeometry.vertices[planeGeometry.faces[i].b].z));
    //     var thirdColor = new THREE.Color(getPlaneColorDependingOnDepth(planeGeometry.vertices[planeGeometry.faces[i].c].z));
    //     planeGeometry.faces[i].vertexColors = [firstColor, secondColor, thirdColor];
    //   }
  }

  /**
   * Returns the color a restricted area is overlayd with. For use with vertex colors of a THREE.PlaneGeometry
   */
  getRestrictedColor(p) {
    return new THREE.Color(this.isPointRestricted(p) ? 0xff0000 : 0);
  }

  /**
   * Returns a (float) flag whether a restricted area is restricted. For use as attribute for THREE.PlaneBufferGeometry
   */
  getRestrictedAtribute(x, y, z) {
    return this.isPointRestricted(new THREE.Vector3(x, y, z)) ? 1.0 : 0.0;
  }

  /**
   * Sets the vertex colors of the ground tile mesh according to whether it is restricted
   */
  colorGroundTileRestricted(planeGeometry, planePos, offsetX, offsetY) {
    // For normal THREE.PlaneGeometry
    // var numberOfFaces = planeGeometry.faces.length;
    // for (var i = 0; i < numberOfFaces; i++) {
    //   var p1 = planePos.clone().add(planeGeometry.vertices[planeGeometry.faces[i].a]);
    //   var p2 = planePos.clone().add(planeGeometry.vertices[planeGeometry.faces[i].b]);
    //   var p3 = planePos.clone().add(planeGeometry.vertices[planeGeometry.faces[i].c]);
    //   planeGeometry.faces[i].vertexColors = [this.getRestrictedColor(p1), this.getRestrictedColor(p2), this.getRestrictedColor(p3)];
    // }
    // planeGeometry.elementsNeedUpdate = true;

    // For THREE.PlaneBufferGeometry
    for (var i = 0; i < planeGeometry.attributes.position.count; i++) {
      var x = planePos.x + planeGeometry.attributes.position.array[i * 3];
      var y = planePos.y + planeGeometry.attributes.position.array[i * 3 + 1];
      var z = planeGeometry.attributes.position.array[i * 3 + 2];
      planeGeometry.attributes.restricted.array[i] = this.getRestrictedAtribute(x, y, z);
    }
    planeGeometry.attributes.restricted.needsUpdate = true;
  }

  /**
   * Sets the vertex colors of all ground tiles according to whether it is restricted
   */
  colorGroundTiles() {
    for (var xi = 0; xi < this.bounds.numTiles.x; xi += 1) {
      for (var yi = 0; yi < this.bounds.numTiles.y; yi += 1) {
        // Get plane that represents the tile at xi, yi
        var planeIndex = yi * this.bounds.numTiles.x + xi;
        var plane = ground.children[planeIndex];

        if (this.bounds.latLongBounds) {
          this.colorGroundTileRestricted(plane.geometry, ground.children[planeIndex].position, xi, yi);
        }
        if (!this.hasAllTextures()) {
          this.colorGroundTileHeight(plane.geometry, xi, yi);
        }
      }
    }
  }

  /**
   * Creates the geometry for a ground tile from the heightmap
   */
  createGroundTileFromHeightmap(offsetX, offsetY, globalTileWidth, globalTileHeight, texture) {
    var tileWidth = this.heightmap.size.x / this.bounds.numTiles.x;
    var tileHeight = this.heightmap.size.y / this.bounds.numTiles.y;

    var numPolygonsX = tileWidth;
    var numPolygonsY = tileHeight;

    if (offsetX < this.bounds.numTiles.x - 1) {
      numPolygonsX++;
    }
    if (offsetY < this.bounds.numTiles.y - 1) {
      numPolygonsY++;
    }


    // For normal THREE.PlaneGeometry
    // var geometry = new THREE.PlaneGeometry(globalTileWidth, globalTileHeight, numPolygonsX - 1, numPolygonsY - 1);
    // for (var yi = 0; yi < numPolygonsY; yi++) {
    //   for (var xi = 0; xi < numPolygonsX; xi++) {
    //     geometry.vertices[xi + yi * numPolygonsX].z = project.terrain.heightmap.getHeightRendering(new THREE.Vector2(offsetX * tileWidth + xi, offsetY * tileHeight + yi));
    //   }
    // }

    // For THREE.PlaneBufferGeometry. Much faster
    var geometry = new THREE.PlaneBufferGeometry(globalTileWidth, globalTileHeight, numPolygonsX - 1, numPolygonsY - 1);
    for (var yi = 0; yi < numPolygonsY; yi++) {
      for (var xi = 0; xi < numPolygonsX; xi++) {
        var vindex = xi + yi * numPolygonsX;
        geometry.attributes.position.array[vindex * 3 + 2] = project.terrain.heightmap.getHeightRendering(new THREE.Vector2(offsetX * tileWidth + xi, offsetY * tileHeight + yi));
      }
    }

    var restrictedArr = new Float32Array(geometry.attributes.position.count);
    geometry.addAttribute('restricted', new THREE.BufferAttribute(restrictedArr, 1));

    var baseMaterial = new THREE.MeshPhongMaterial({
      vertexColors: THREE.VertexColors,
      onBeforeCompile: (shader) => {
        // Set the shader such that the grid is overlayed on the ground
        shader.fragmentShader = terrainGridFragmentShader;
        shader.vertexShader = terrainGridVertexShader;
        shader.uniforms.gridInvisibleBelowZero = new THREE.Uniform(true);
      },
      map: texture
    });

    var plane = new THREE.Mesh(geometry, baseMaterial);
    return plane;
  }

  /**
   * Creates the ground from the heightmap
   */
  createGroundFromHeightmap() {
    ////////// Terrain ////////////
    var globalHeightmapSizeX = this.heightmap.size.x * this.heightmap.pixelWidth;
    var globalHeightmapSizeY = this.heightmap.size.y * this.heightmap.pixelWidth;

    var globalTileWidth = globalHeightmapSizeX / this.bounds.numTiles.x;
    var globalTileHeight = globalHeightmapSizeY / this.bounds.numTiles.y;

    ground = createEntity(['Transform', 'ReceiveShadow'], new THREE.Object3D(), {}).addToScene();

    // Iterate over the tiles and create their geometry from the heightmap
    for (var xi = 0; xi < this.bounds.numTiles.x; xi += 1) {
      for (var yi = 0; yi < this.bounds.numTiles.y; yi += 1) {
        var texture = null;
        if (this.hasTextures()) {
          if (this.hasAllTextures()) {
            texture = this.textures[xi + this.bounds.numTiles.x * yi];
          } else {
            console.error(`Not enough textures: Only ${this.textures.length} for ${this.bounds.numTiles.x}x${this.bounds.numTiles.y} tiles`);
          }
        }
        var plane = this.createGroundTileFromHeightmap(xi, yi, globalTileWidth, globalTileHeight, texture);
        plane.position.x = globalTileWidth / 2.0 + xi * globalTileWidth;
        plane.position.y = -(globalTileHeight / 2.0 + yi * globalTileWidth);
        ground.add(plane);
      }
    }
    this.colorGroundTiles();

    ////////// Grid highlight element ////////////
    // This entity is a circle that follows the mouse in certain editing modes and shows with its color whether e.g. creating an object
    // is possible at that position
    this.gridHighlightElement = createEntity(['Transform', 'GridPosition', 'GridPositionByMousePosition', 'Circle', 'Material', 'ColorCodeObjectPlaceability'], new THREE.Object3D(), {
      'Transform': {
        scale: new THREE.Vector3(this.heightmap.gridSpacing / 2.0, this.heightmap.gridSpacing / 2.0, 1)
      },
      'GridPosition': {
        heightMode: 'elevateOnLand',
        heightOffset: 0.5
      },
      'Material': {
        color: new THREE.Color(0, 1, 0),
        transparent: true,
        opacity: 0.5
      },
      'GridPositionByMousePosition': {
        lockOnHoveredObjects: true
      }
    }).addToScene();
    this.gridHighlightElement.visible = false;
  }
}

/**
 * ArrayTerrain and HeightmapFileTerrain are old and wont work at the moment. They'd have to be adjusted, but the idea should work
 */
// class ArrayTerrain extends Terrain {
//   constructor(terrainOptions) {
//     super(terrainOptions);
//     this.heightArray = terrainOptions.heightArray;
//   }

//   load(dontResetCamera) {
//     this.createTerrainGeometry(this.heightArray, 1, 1, null); // Load array, resulting in a 1x1 tile grid with no textures (null)
//     this.fullyLoaded = true;
//   }
// }

// class HeightmapFileTerrain extends Terrain {
//   constructor(terrainOptions) {
//     super(terrainOptions);
//     this.url = terrainOptions.url;
//   }

//   load(dontResetCamera) {
//     Jimp.read(this.url)
//       .then(im => {
//         // Create two-dimensional array with default values. 
//         var heightmapDataObject = {
//           heightmapData: Array(im.bitmap.height).fill(0).map(x => Array(im.bitmap.width).fill(-1))
//         };
//         this.parseImagePixels(im, heightmapDataObject, 0, 0, false, im.bitmap.width, null);
//         this.createTerrainGeometry(heightmapDataObject.heightmapData, 1, 1, null);
//         this.fullyLoaded = true;
//       })
//       .catch(err => {
//         console.error(err);
//       });
//   }
// }

/**
 * Special class for a terrain loaded from mapbox
 */
class MapboxTerrain extends Terrain {
  constructor(terrainOptions) {
    // Ensure no scaling of height data from mapbox (resembles real height already)
    terrainOptions.heightFactor = 1.0;

    // Compute pixel width from latitude and zoom level and chosen resolution, 256 is the standard tile length
    terrainOptions.pixelWidth = MapboxTerrain.metersPerMapboxPixel(terrainOptions.tileOptions.zoom, terrainOptions.boundingPolygon[0].lat) * 256 / terrainOptions.tileOptions.resolution;

    super(terrainOptions);
    if (terrainOptions.bounds) {
      this.bounds = new TileBounds(terrainOptions.bounds, terrainOptions.tileOptions);
    } else {
      this.bounds = new TileBounds(Terrain.boundsFromPolygon(terrainOptions.boundingPolygon), terrainOptions.tileOptions);
    }
    this.tileOptions = terrainOptions.tileOptions;
  }

  /**
   * Computes the mapbox url to download a tile with coordinates x/y at zoom level zoom and resolution resolution. Return the terrain/color image
   * url depending on img.
   * 
   * Partly from https://blog.mapbox.com/bringing-3d-terrain-to-the-browser-with-three-js-410068138357
   */
  assembleUrl(img, x, y, zoom, resolution) {
    var tileset = img ? 'mapbox.satellite' : 'mapbox.terrain-rgb'; //
    var ending = resolution == 512 ? '@2x.pngraw' : '.pngraw';

    //domain sharding
    var serverIndex = 2 * (x % 2) + y % 2;
    var server = ['a', 'b', 'c', 'd'][serverIndex];
    var token = 'pk.eyJ1IjoiZ3Jvc3Nqb2hhbm5lcyIsImEiOiJjamFsNWh4b3UybHJwMnFsZDVrMnJleGd6In0.ZCtiPI5-yjXn7UBs8zIpRg';
    return 'https://' + server + '.tiles.mapbox.com/v4/' + tileset + '/' + [zoom, x, y].join('/') + ending + '?access_token=' + token;
  }

  /**
   * Computes meters per standard mapbox pixel from latitude and zoom level
   *
   * See http://wiki.openstreetmap.org/wiki/Zoom_levels, 
   * for forumula http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Lon..2Flat._to_tile_numbers
   */
  static metersPerMapboxPixel(zoomLevel, latitude) {
    return 40075000 * Math.cos(latitude / 180.0 * Math.PI) / Math.pow(2.0, zoomLevel + 8);
  }

  /**
   * Loads tile (defined by tileData) heightdata and texture from mapbox server, parses the image pixels
   * into the heightmap (heightmapDataObject), loads the texture
   */
  loadTile(heightmapDataObject, tileData, resolution, texResolution) {
    return new Promise((resolve, reject) => {
      var url = this.assembleUrl(false, tileData.x, tileData.y, tileData.zoom, resolution);
      this.requestBinary(url)
        .then(this.parsePNGBuffer) // Parse PNG -> convert to buffer of colors (4 bytes per pixel)
        .catch(err => {
          reject(err);
        })
        .then(im => {
          var urlTex = this.assembleUrl(true, tileData.x, tileData.y, tileData.zoom, texResolution);
          var textureMap = this.parseImagePixels(im, heightmapDataObject, tileData.offsetX, tileData.offsetY, true, resolution, urlTex);
          resolve(textureMap);
        });
    });
  }

  /**
   * Loads the terrain from mapbox given bounds and tile options
   */
  load(dontResetCamera) {
    // Create two-dimensional array with default values. heightmapDataObject is used to pass the heightmap by reference
    var heightmapDataObject = {
      heightmapData: Array(this.tileOptions.resolution * this.bounds.numTiles.y).fill(0).map(x => Array(this.tileOptions.resolution * this.bounds.numTiles.x).fill(-1))
    };

    // Holds the promises for all tile load subcalls
    var promises = [];
    for (var y = this.bounds.min.y; y <= this.bounds.max.y; y += 1) {
      for (var x = this.bounds.min.x; x <= this.bounds.max.x; x += 1) {
        // Load a tile at x/y (with offsetX/offsetY from upper left tile)
        promises.push(this.loadTile(heightmapDataObject, {
          zoom: this.tileOptions.zoom,
          x: x,
          y: y,
          offsetX: x - this.bounds.min.x,
          offsetY: y - this.bounds.min.y
        }, this.tileOptions.resolution, this.tileOptions.texResolution));
      }
    }

    var scope = this;
    // React to all promises being loaded (heightmap fully loaded, textures loaded), then create the terrain object
    Promise.all(promises).then((textures) => {
      scope.textures = textures;
      scope.createTerrainGeometry(heightmapDataObject.heightmapData, scope.bounds.numTiles.x, scope.bounds.numTiles.y);
      if (!dontResetCamera) {
        resetCameraPosition();
        setCamera(this.heightmap.center);
      }
      scope.fullyLoaded = true;
    });
  }
}
