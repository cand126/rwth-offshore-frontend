// Global entity manager object
var entityManager = new MyEntityManager();

// Dictionary of scene graph objects with their entity id as key
var sceneGraphEntities = {};

/**
 * Creates an entity with components [components] with the respective states [states].
 * Return [sceneGraphObject] with the entityId and some convenience functions attached
 */
function createEntity(components, sceneGraphObject, states) {
  // Create the entity with components
  var entityId = entityManager.createEntity(components);

  // Add the pointer to the scene graph object to the sceneGraphEntities dictionary with key entityId
  sceneGraphEntities[entityId] = sceneGraphObject;
  sceneGraphObject.entityId = entityId;

  // Add the component states
  if (states && typeof states === "object") {
    Object.keys(states).forEach(function(key) {
      updateComponentDataForEntity(key, entityId, states[key]);
    });
  }

  // Add some convenience function for...
  sceneGraphObject.addToScene = function() { // ... adding the object to the scene graph root
    scene.add(this);
    return this;
  };
  sceneGraphObject.addToObject = function(obj) { // ... adding the object to another object
    obj.add(this);
    return this;
  };
  sceneGraphObject.removeFromScene = function() { // ... removing the object from the scene graph
    removeEntityFromScene(this);
    return this;
  };
  sceneGraphObject.destroy = function() { // ... destroying an entity
    destroyEntity(this);
  };
  sceneGraphObject.hasComponent = function(component) { // ... checking whether an entity has a component
    return entityHasComponent(this, component);
  };
  sceneGraphObject.getComponentData = function(component) { // ... getting the state of a component
    return getComponentDataForEntity(component, this);
  };
  sceneGraphObject.addComponents = function(components, states) { // ... adding a component with its state to the entity
    addComponentsToEntity(this, components, states);
    return this;
  };
  return sceneGraphObject;
}

/**
 * Allows to call the following functions like getComponentDataForEntity(..., 1) and getComponentDataForEntity(..., {entityId: 1, ...})
 */
function parseEntityId(obj) {
  return Number.isInteger(obj) ? obj : obj.entityId;
}

/**
 * Updates the state of a component of the given entity
 */
function updateComponentDataForEntity(component, entity, updateObj) {
  var entityId = parseEntityId(entity);
  entityManager.updateComponentDataForEntity(component, entityId, updateObj);
}

/**
 * Returns the state of a component of the given entity
 */
function getComponentDataForEntity(component, entity) {
  var entityId = parseEntityId(entity);
  return entityManager.getComponentDataForEntity(component, entityId);
}

/**
 * Adds components together with their states to the given entity
 */
function addComponentsToEntity(entity, components, states) {
  var entityId = parseEntityId(entity);
  entityManager.addComponentsToEntity(components, entityId);

  if (states && typeof states === "object") {
    Object.keys(states).forEach(function(key) {
      updateComponentDataForEntity(key, entityId, states[key]);
    });
  }
}

/**
 * Gets the first child (entity) of the entity with the given component
 */
function getEntityChildWithComponent(entity, component) {
  var entityId = parseEntityId(entity);
  var entitySceneGraphObject = sceneGraphEntities[entityId];
  return getChildWithComponent(entitySceneGraphObject, component);
}

function getChildWithComponent(entitySceneGraphObject, component) {
  for (var i = 0; i < entitySceneGraphObject.children.length; i++) {
    var child = entitySceneGraphObject.children[i];
    if (child.hasOwnProperty('entityId') && entityHasComponent(child.entityId, component)) {
      return child;
    } else {
      var childWithComponent = getChildWithComponent(child, component);
      if (childWithComponent != null) {
        return childWithComponent;
      }
    }
  }
  return null;
}

/**
 * Removes a component from the entity
 */
function removeComponentsFromEntity(components, entity) {
  var entityId = parseEntityId(entity);
  entityManager.removeComponentsFromEntity(components, entityId);
}

/**
 * Returns whether the entity has the component
 */
function entityHasComponent(entity, component) {
  var entityId = parseEntityId(entity);
  return entityManager.entityHasComponent(entityId, component);
}

/**
 * Removes the entity from the scene graph
 */
function removeEntityFromScene(entity) {
  var entityId = parseEntityId(entity);
  var entitySceneGraphObject = sceneGraphEntities[entityId];
  delete sceneGraphEntities[entityId];
  scene.remove(entitySceneGraphObject);
}

/**
 * Destroys the entity, thus removes it and its children from the scene graph and the entity manager.
 * For Connectables, all cables ending in it are destroyed too.
 */
function destroyEntity(entity) {
  var entityId = parseEntityId(entity);
  var entitySceneGraphObject = sceneGraphEntities[entityId];

  if (entitySceneGraphObject) {
    for (var i = 0; i < entitySceneGraphObject.children.length; i++) {
      if (Number.isInteger(entitySceneGraphObject.children[i].entityId)) {
        entitySceneGraphObject.children[i].destroy();
      }
    }
  }

  if (entityHasComponent(entity, 'Connectable')) {
    cleanUpCables(getComponentDataForEntity('Connectable', entity).id);
  }

  entityManager.removeEntity(entityId);
  removeEntityFromScene(entity);
}

/**
 * Destroys all cables ending in [removedId]
 */
function cleanUpCables(removedId) {
  var cables = entityManager.getAllComponentsData(['CableConnection']);
  for (var i = 0; i < cables.length; i++) {
    let cable = cables[i];
    if (cable.CableConnection.pointA == removedId || cable.CableConnection.pointB == removedId) {
      // console.log(`Deleting cable ${cable.CableConnection.pointA} <-> ${cable.CableConnection.pointB}`);
      cable.getSceneGraphObject().destroy();
    }
  }
}
