// Several global objects needed to access the renderer, the render stages etc from everywhere
var canvasContainer, renderer, composer, outlinePass, selectedObjectOutlinePass, effectFXAA;

/**
 * Init the renderer and the render stages
 */
function initRenderer(_canvasContainer) {
  canvasContainer = _canvasContainer;

  // The logarithmic depth buffer is useful if geometry in short and long distances should be
  // rendered without z-fighting.

  // As of now, we cannot handle logarithmic depth buffers in our terrain/grid shader if the OpenGL
  // EXT_frag_depth extension is not available. Thus we use the standard depth buffer in that case
  var testCanvas = document.createElementNS('http://www.w3.org/1999/xhtml', 'canvas');
  var logarithmicDepthBufferExtensionGLAvailable = testCanvas.getContext('webgl').getExtension('EXT_frag_depth') != null;

  renderer = new THREE.WebGLRenderer({
    antialias: false, // FXAA is used for antialiasing in last step of render pipeline
    logarithmicDepthBuffer: logarithmicDepthBufferExtensionGLAvailable
  });
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  // Will be overridden later to the actual render size
  var width = 100;
  var height = 100;
  renderer.setSize(width, height);
  renderer.sortObjects = false;

  renderer.autoClear = false;
  renderer.setClearColor(0xffffff);
  renderer.setClearAlpha(0.0);

  // Render pipeline 
  composer = new THREE.EffectComposer(renderer);

  // Renderpass
  var renderPass = new THREE.RenderPass(scene, camera);
  composer.addPass(renderPass);

  // Outline pass for hovering objects
  outlinePass = new THREE.OutlinePass(new THREE.Vector2(renderer.getSize().width, renderer.getSize().height), scene, camera);
  outlinePass.edgeStrength = 1.0;
  outlinePass.edgeGlow = 0.1;
  outlinePass.edgeThickness = 0.1;
  // outlinePass.pulsePeriod = 0.0;
  outlinePass.visibleEdgeColor = new THREE.Color(0.5, 1, 0.5);
  // outlinePass.hiddenEdgeColor = new THREE.Color(1, 0, 0);
  composer.addPass(outlinePass);

  // Outline pass for showing selected objects
  selectedObjectOutlinePass = new THREE.OutlinePass(new THREE.Vector2(renderer.getSize().width, renderer.getSize().height), scene, camera);
  selectedObjectOutlinePass.edgeStrength = 2.0;
  selectedObjectOutlinePass.edgeGlow = 0.1;
  selectedObjectOutlinePass.edgeThickness = 0.5;
  selectedObjectOutlinePass.pulsePeriod = 3.0;
  selectedObjectOutlinePass.visibleEdgeColor = new THREE.Color(1, 1, 0.1);
  // selectedObjectOutlinePass.hiddenEdgeColor = new THREE.Color(1, 0, 0);
  composer.addPass(selectedObjectOutlinePass);

  // FXAA antialiasing pass
  effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
  effectFXAA.uniforms['resolution'].value.set(1 / renderer.getSize().width, 1 / renderer.getSize().height);
  effectFXAA.renderToScreen = true; // Last step in render pipeline
  composer.addPass(effectFXAA);
}
