// Holds a possible debug camera helper
var cameraHelper;

// Global handle for the sun light
var sunLight;

// Hold the current time of the day, only 8 <= timeOfDay < 16
var timeOfDay = 11;

/**
 * Initializes the scene lighting
 */
function initLights() {
  var light = new THREE.AmbientLight(0x555555, 1);
  scene.add(light);

  sunLight = new THREE.DirectionalLight(0xffffff, 1, 100);
  sunLight.position.set(-100, -500, 100);
  sunLight.castShadow = true;

  sunLight.shadow.camera.visible = false;

  sunLight.shadow.mapSize.width = 1024;
  sunLight.shadow.mapSize.height = 1024;
  sunLight.shadow.camera.near = 10;
  sunLight.shadow.camera.far = 1000;
  sunLight.shadow.camera.left = -500;
  sunLight.shadow.camera.right = 500;
  sunLight.shadow.camera.top = 500;
  sunLight.shadow.camera.bottom = -500;

  sunLight.target.position.set(0, 0, 0);
  scene.add(sunLight.target);

  hemiLight = new THREE.HemisphereLight(0xaaaaff, 0xffffff, 0.4);
  // hemiLight.color.setRGB(0.7, 0.7, 1);
  hemiLight.groundColor.setHSL(0.095, 1, 0.75);
  hemiLight.position.set(0, 0, 500);
  hemiLight.rotation.x = Math.PI / 2.0;
  scene.add(hemiLight);
  hemiLightHelper = new THREE.HemisphereLightHelper(hemiLight, 10);
  // scene.add(hemiLightHelper);

  scene.add(sunLight);

  compassLight = new THREE.DirectionalLight(0xffffff, 1, 10);
  compassLight.position.set(5, -100, 10);

  compassLight2 = new THREE.DirectionalLight(0xffffff, 1, 10);
  compassLight2.position.set(100, -100, 10);

  compassScene.add(compassLight);
  compassScene.add(compassLight2);

  var showDebugCamera = false;
  if (showDebugCamera) {
    cameraHelper = new THREE.CameraHelper(sunLight.shadow.camera);
    scene.add(cameraHelper);
  }

  scene.fog = new THREE.FogExp2(0xd4e9fc, 0.0001);
}

/**
 * Sets the sun position according to the current time
 */
function setSunPosition() {
  if (sunLight) {
    var cameraCenterSlightlyMovedTowardsSun = new THREE.Vector3(cameraCenter.x, cameraCenter.y - 200, cameraCenter.z);
    sunLight.position.copy(cameraCenterSlightlyMovedTowardsSun);

    sunLight.position.x += 3.0 * 100.0 * Math.sin((timeOfDay - 12.0) / 8.0 * Math.PI);
    sunLight.position.y += -500;
    sunLight.position.z += 1.5 * 200.0 * Math.cos((timeOfDay - 12.0) / 8.0 * Math.PI);

    sunLight.shadow.camera.up = new THREE.Vector3(0, 0, 1);
    sunLight.target.position.copy(cameraCenterSlightlyMovedTowardsSun);
    sunLight.target.position.y -= 100;
  }
}
