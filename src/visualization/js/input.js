// Global mouse state
var mouse = {
  pos: new THREE.Vector2(), // screen position
  dragStart: new THREE.Vector2(), // screen position where a drag started
  gridPos: new THREE.Vector2(), // grid position the mouse last moved over
  clicked: false,
  dragged: false,
  changed: false, // If a change in mouse state occured (mouse move down or up since the last frame)
  lastClick: -1, // Timestamp of the last click
  lastScrolledUp: false // Whether we last scrolled up or down
};

// Global raycaster object (to not create this again and again)
var raycaster = new THREE.Raycaster();

// Global controls object (for orbit controls plugin)
var controls;

// Global object holding the dagui debug gui
var datgui;

// Global object holding the global settings
var globalSettings = {
  gridVisible: true,
  groundVisible: true,
  seaplaneVisible: true,
  gridLineWidth: 1.0,
  windDistributionVisible: true,
  liveDataEnabled: false,
  globalInfoWindowVisible: true,
  fpsWindowEnabled: false,
  extraControlsMenuEnabled: false,
  updateComponentsFlag: true,
  renderFlag: true,
  resolutionMode: 0,
  spamLiveData: true
};

/**
 * Initializes the debug GUI (menu in the top right), which is from the datGUI library. 
 */
function initGUI() {
  // Init gui
  datgui = new dat.GUI();
  datgui.domElement.id = 'datgui';
  // Invisible at first
  document.querySelector('.dg.ac').style.display = "none";

  var weatherFolder = datgui.addFolder('Weather');
  weatherFolder.add(window, "windspeed", 0, 20);
  weatherFolder.add(window, "windDirection", 0, 360.0);
  weatherFolder.add(window, "timeOfDay", 8, 16);

  var visibilityFolder = datgui.addFolder('Visibility');
  visibilityFolder.add(globalSettings, 'gridVisible');
  visibilityFolder.add(globalSettings, 'gridLineWidth', 0.1, 10.0);
  visibilityFolder.add(globalSettings, 'groundVisible');
  visibilityFolder.add(globalSettings, 'seaplaneVisible');
  visibilityFolder.add(globalSettings, 'windDistributionVisible');

  var projectFolder = datgui.addFolder('Live');
  projectFolder.add(window, "sendRealWindDistribution");
  projectFolder.add(window, "sendRandomWindDistributionsTimer");
  projectFolder.add(window, "sendRandomWindmillDirections");
  projectFolder.add(window, "sendRandomWindmillDirectionsTimer");
  projectFolder.add(window, "sendRandomWindmillSpeeds");
  projectFolder.add(window, "sendRandomWindmillSpeedsTimer");

  datgui.add(globalSettings, "renderFlag");
  datgui.add(globalSettings, "updateComponentsFlag");
}

/**
 * Inits OrbitControls (part of threejs) and event listeners
 */
function initControls() {
  controls = new THREE.OrbitControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.25;
  controls.maxDistance = 3000;
  controls.minDistance = 1;
  controls.enablePan = false;
  controls.maxPolarAngle = 0.5 * Math.PI - 0.1;

  canvas.addEventListener('mousedown', visu_onDocumentMouseDown, false);
  canvas.addEventListener('mouseup', visu_onDocumentMouseUp, false);
  canvas.addEventListener('touchstart', visu_onDocumentTouchStart, false);
  canvas.addEventListener('touchend', visu_onDocumentTouchEnd, false);
  canvas.addEventListener('mousemove', visu_onDocumentMouseMove);
  canvas.addEventListener('touchmove', visu_onDocumentTouchMove);
  canvas.addEventListener('wheel', visu_onMouseWheel, false);
  document.addEventListener('keydown', visu_onDocumentKeyDown, false);
  window.addEventListener('resize', visu_onWindowResize, false);
  canvas.addEventListener('contextmenu', visu_onContextMenu, false);
}

function visu_onContextMenu(event) {
  event.preventDefault();
}

/**
 * Called whenever the window resizes or the canvas has changed e.g. due to the sidebar being collapsed
 */
function visu_onWindowResize() {
  if (!canvasContainer) { // Crashes otherwise in safari
    return;
  }

  var resolutionFactor = 1.0;
  if (globalSettings.resolutionMode == 1) {
    resolutionFactor = 0.8;
  } else if (globalSettings.resolutionMode == 2) {
    resolutionFactor = 0.6;
  }

  // If a lower resolution is chosen (looks really bad, adjust the canvas/viewport size accordingly)
  var width = (canvasContainer.clientWidth - 0) * resolutionFactor || 1;
  var height = (canvasContainer.clientHeight - 0) * resolutionFactor || 1;
  var devicePixelRatio = window.devicePixelRatio || 1;

  camera.aspect = width / height;
  camera.updateProjectionMatrix();

  compassCamera.left = -width / 1000;
  compassCamera.right = width / 1000;
  compassCamera.top = height / 1000;
  compassCamera.bottom = -height / 1000;
  compassCamera.updateProjectionMatrix();

  // Show compass a little smaller on mobile
  entityManager.getAllComponentsData(['Transform', 'RelativeToCamera']).forEach(compass => {
    if (canvasContainer.clientWidth < 800) {
      compass.Transform.scale.set(0.08, 0.08, 0.08);
      compass.RelativeToCamera.position.set(-0.7, 0.8);
    } else {
      compass.Transform.scale.set(0.15, 0.15, 0.15);
      compass.RelativeToCamera.position.set(-0.8, 0.7);
    }
  });

  renderer.setSize(width, height);
  composer.setSize(width, height);

  effectFXAA.uniforms['resolution'].value.set(1 / renderer.getSize().width, 1 / renderer.getSize().height);
}

/**
 * keypress event callback
 */
function visu_onDocumentKeyDown(event) {
  if (!project || !project.terrain || !threeJSShouldRender) return;

  var keyCode = event.which;
  // console.log(keyCode);
  if (keyCode == 65 || keyCode == 27) { // key a or esc
    switchMode('mode_default');
  } else if (keyCode == 83) { // key s
    switchMode('mode_createWindmill');
  } else if (keyCode == 68) { // key d
    switchMode('mode_createPowerstation');
  } else if (keyCode == 70) { // key f
    switchMode('mode_createWiring');
  } else if (keyCode == 82) { // key r
    // switchMode('mode_restrictAreaCircle');
  }
}

/**
 * Goes up the scene graph until an ancestor with the given component is found.
 * Used for the mouse ray intersection which return the deepest scene graph object which is
 * intersected (a child of whatever we want to find)
 */
function findParentWithComponent(obj, component) {
  if (obj.hasOwnProperty('entityId') && entityManager.entityHasComponent(obj.entityId, component)) {
    return obj;
  } else {
    if (obj.parent) {
      return (findParentWithComponent(obj.parent, component));
    } else {
      return null;
    }
  }
}

/**
 * Casts a ray from the mouse pointer and intersects with the terrain/ocean level. Returns the nearest
 * grid position and the world position of the intersection
 */
function checkIntersectionWithGrid() {
  if (seaplane && ground) {
    raycaster.setFromCamera(mouse.pos, camera);
    var intersects = raycaster.intersectObjects([seaplane, ground], true);
    if (intersects.length > 0) {
      let gridPos = project.terrain.heightmap.worldCoordinatesToGridCoordinates(intersects[0].point);
      return {
        gridPos: gridPos,
        worldPos: intersects[0].point
      };
    }
  }
  return {
    gridPos: null,
    worldPos: null
  };
}

/**
 * Compute the mouse position in Normalized screen coordinates [-1, 1] to allow unprojection to world space
 */
function computeMousePos(event) {
  mouse.pos.x = ((event.clientX - canvas.offsetLeft) / canvas.clientWidth) * 2 - 1;
  mouse.pos.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;
}

///////////////////////// Mouse/Touch Down Events ///////////////////////////////////

function visu_onDocumentTouchStart(event) {
  event.preventDefault();
  event.clientX = event.touches[0].clientX;
  event.clientY = event.touches[0].clientY;
  event.isTouch = true;
  visu_onDocumentMouseDown(event);
}

function visu_onDocumentMouseDown(event) {
  if (!project || !project.terrain) return;

  event.preventDefault();
  computeMousePos(event);

  mouse.clicked = true;
  mouse.changed = true;
  mouse.dragStart.copy(mouse.pos);
  mouse.rightClick = event.which == 3;

  var intersection = checkIntersectionWithGrid();
  mouse.gridPos = intersection.gridPos;
  mouse.worldPos = intersection.worldPos;

  var now = Date.now();
  if (mouse.lastClick > 0 && now - mouse.lastClick < 300 && (!event.touches || event.touches.length == 1)) {
    event.isTouch ? getCurrentEditMode().touchDoubleclickDown(mouse) : getCurrentEditMode().mouseDoubleclickDown(mouse);
    mouse.lastClick = -1;
  } else {
    event.isTouch ? getCurrentEditMode().touchDown(mouse) : getCurrentEditMode().mouseDown(mouse);
  }
  mouse.lastClick = Date.now();
}

///////////////////////// Mouse/Touch Move Events ///////////////////////////////////

function visu_onDocumentTouchMove(event) {
  event.preventDefault();
  event.clientX = event.changedTouches[0].clientX; // clientX?
  event.clientY = event.changedTouches[0].clientY;
  event.isTouch = true;
  visu_onDocumentMouseMove(event);
}

function visu_onDocumentMouseMove(event) {
  if (!project || !project.terrain) return;

  computeMousePos(event);
  mouse.changed = true;
  if (mouse.clicked && (mouse.dragStart.x != mouse.pos.x || mouse.dragStart.y != mouse.pos.y)) {
    mouse.dragged = true;
  }

  var intersection = checkIntersectionWithGrid();
  mouse.gridPos = intersection.gridPos;
  mouse.worldPos = intersection.worldPos;

  event.isTouch ? getCurrentEditMode().touchMove(mouse) : getCurrentEditMode().mouseMove(mouse);
}

///////////////////////// Mouse/Touch Up Events ///////////////////////////////////

function visu_onDocumentTouchEnd(event) {
  event.preventDefault();
  event.clientX = event.changedTouches[0].clientX;
  event.clientY = event.changedTouches[0].clientY;
  event.isTouch = true;
  visu_onDocumentMouseUp(event);
}

function visu_onDocumentMouseUp(event) {
  if (!project || !project.terrain) return;
  event.preventDefault();

  computeMousePos(event);

  event.isTouch ? getCurrentEditMode().touchUp(mouse) : getCurrentEditMode().mouseUp(mouse);

  mouse.clicked = false;
  mouse.dragged = false;
  mouse.changed = true;
  mouse.rightClick = event.which == 3;
}

///////////////////////// Mouse/Touch Wheel Events ///////////////////////////////////

function visu_onMouseWheel(ecent) {
  event.preventDefault();
  event.stopPropagation();
  mouse.lastScrolledUp = event.deltaY < 0;
  getCurrentEditMode().mouseScrolled(mouse);
}
