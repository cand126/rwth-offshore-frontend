// For sending a limited number of mock live updates, e.g. in sendRandomWindDistributionsTimer, this hold a handle for the used timer
var liveUpdateTimer = null;

// The number of remaining mock live updates to send
var numRemainingLiveUpdates = 0;

// Spam live data enabled windmills with random mock live data
setInterval(() => {
  if (project && threeJSShouldRender && globalSettings.liveDataEnabled && globalSettings.spamLiveData) {
    sendRandomWindDistributions();
    sendRandomWindmillDirections();
    sendRandomWindmillSpeeds();
  }
}, 5000);

/**
 * Send a real example wind distribution to the windmills (from the offshore project repo)
 */
function sendRealWindDistribution() {
  var entities = entityManager.getAllComponentsData(['LiveData']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    let liveId = entity.LiveData.id;
    project.liveDataIncoming([{
      id: liveId,
      type: 'WindDistribution',
      distribution: [{
        angle: 0,
        speed: 8.3658528889211201,
      }, {
        angle: 0.52359877559829882,
        speed: 7.9874741178858519,
      }, {
        angle: 1.0471975511965976,
        speed: 9.1818171921752505,
      }, {
        angle: 1.5707963267948966,
        speed: 11.240903246716702,
      }, {
        angle: 2.0943951023931953,
        speed: 12.022442351070799,
      }, {
        angle: 2.6179938779914944,
        speed: 10.677232186270308,
      }, {
        angle: 3.1415926535897931,
        speed: 9.4887929782027225,
      }, {
        angle: 3.6651914291880923,
        speed: 12.738048505093239,
      }, {
        angle: 4.1887902047863905,
        speed: 12.687229350611714,
      }, {
        angle: 4.7123889803846897,
        speed: 11.856609951616097,
      }, {
        angle: 5.2359877559829888,
        speed: 11.433076426090619,
      }, {
        angle: 5.7595865315812871,
        speed: 10.671981762060591,
      }]
    }]);
  }
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Send random wind distributions to the windmills
 */
function sendRandomWindDistributions() {
  var entities = entityManager.getAllComponentsData(['LiveData']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    let liveId = entity.LiveData.id;

    var randLength = getRandomInt(5, 20);
    var distribution = new Array(randLength);
    for (var j = 0; j < randLength; j++) {
      distribution[j] = {
        angle: Math.random() * Math.PI * 2.0,
        speed: Math.random() * 20.0
      };
    }
    distribution = distribution.sort((a, b) => a.angle - b.angle);

    project.liveDataIncoming([{
      id: liveId,
      type: 'WindDistribution',
      distribution: distribution
    }]);
  }
}

/**
 * Send random windmill directions to the windmills
 */
function sendRandomWindmillDirections() {
  var entities = entityManager.getAllComponentsData(['LiveData']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    let liveId = entity.LiveData.id;

    var randDirection = Math.random() * Math.PI * 2.0;

    project.liveDataIncoming([{
      id: liveId,
      type: 'Direction',
      direction: randDirection
    }]);
  }
}

/**
 * Send random wind speeds to the windmills
 */
function sendRandomWindmillSpeeds() {
  var entities = entityManager.getAllComponentsData(['LiveData']);
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i];
    let liveId = entity.LiveData.id;

    var randSpeed = Math.random() * 20.0;

    project.liveDataIncoming([{
      id: liveId,
      type: 'Speed',
      speed: randSpeed
    }]);
  }
}


/**
 * Calls func 10 times, every 2 seconds
 */
function sendRandomLiveUpdatesTimer(func) {
  if (liveUpdateTimer) {
    clearInterval(liveUpdateTimer);
    liveUpdateTimer = null;
  }

  numRemainingLiveUpdates = 10;
  liveUpdateTimer = window.setInterval(() => {
    if (numRemainingLiveUpdates > 0) {
      func();
      numRemainingLiveUpdates--;
    } else {
      clearInterval(liveUpdateTimer);
      liveUpdateTimer = null;
    }
  }, 2000);
}

function sendRandomWindDistributionsTimer() {
  sendRandomLiveUpdatesTimer(sendRandomWindDistributions);
}

function sendRandomWindmillDirectionsTimer() {
  sendRandomLiveUpdatesTimer(sendRandomWindmillDirections);
}

function sendRandomWindmillSpeedsTimer() {
  sendRandomLiveUpdatesTimer(sendRandomWindmillSpeeds);
}
