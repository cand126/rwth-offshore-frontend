class Project {
  constructor(projectOptions, frontendVisualizationComponent) {
    this.terrainOptions = projectOptions.terrainOptions;
    this.objects = projectOptions.objects || [];

    // Holds the terrain object
    this.terrain = null;

    // Points to the visualization component, used to trigger project uploads
    this.frontendVisualizationComponent = frontendVisualizationComponent || null;
  }

  /**
   * This is the init method after creating the project.
   * It will initiate the terrain loading
   */
  load(dontResetCamera) {
    // Ensures this is always true or false
    var _dontResetCamera = !!dontResetCamera;

    // Ensures the mode is reset
    switchMode('mode_default');

    this.loadTerrain(_dontResetCamera);
    this.loadObjects();
  }

  /**
   * Loads the terrain specified in terrainOptions
   */
  loadTerrain(dontResetCamera) {
    // If the terrain exists, destroy it
    if (this.terrain) {
      this.terrain.destroy();
      this.terrain = null;
    }

    if (this.terrainOptions.type == 'mapbox') {
      this.terrain = new MapboxTerrain(this.terrainOptions);
      var numx = this.terrain.bounds.numTiles.x;
      var numy = this.terrain.bounds.numTiles.y;

      // Safeguard for really large projects
      if (numx * numy > 300) {
        if (confirm(`The number of mapbox tiles that need to be loaded are rather high (${numx}*${numy}=${numx*numy}). Are you sure you want to load the visualization, as it could slow down your computer significantly! You can also try to lower the mapbox zoom level in the project options or create a project with a smaller bounding polygon.\n\nIf you still want to load the project, click OK`)) {
          this.terrain.load(dontResetCamera);
        } else {
          this.frontendVisualizationComponent.set('projectWaitingToBeLoaded', true);
        }
      } else {
        this.terrain.load(dontResetCamera);
      }
    }
    // These are not fully functional at the moment, see terrain.js
    /* else if (this.terrainOptions.type == 'array') {
          this.terrain = new ArrayTerrain(this.terrainOptions);
          this.terrain.load(dontResetCamera);
        } else if (this.terrainOptions.type == 'heightmapfile') {
          this.terrain = new HeightmapFileTerrain(this.terrainOptions);
          this.terrain.load(dontResetCamera);
        }*/
  }

  loadObjects() {
    this.loadProjectObjects(this.objects);
  }

  clearProjectObjects() {
    entityManager.getAllComponentsData(['CableConnection']).forEach(entity => {
      destroyEntity(entity);
    });

    entityManager.getAllComponentsData(['Connectable']).forEach(entity => {
      destroyEntity(entity);
    });

    currentConnectableId = 0;
  }

  /**
   * Load the project objects (windmills, powerstations, cables) from the project options
   */
  loadProjectObjects(projectObjects) {
    this.clearProjectObjects();

    projectObjects.filter(object => object.type == 'Windmill').forEach(object => {
      let windmill = createPlaceableWindmill(new THREE.Vector2(object.x, object.y)).addToScene();
      finishWindmill(windmill, {
        'Connectable': {
          id: object.id
        },
        'Windmill': {
          efficiency: object.efficiency
        }
      });
      if (object.hasOwnProperty('liveDataId')) {
        windmill.addComponents(['LiveData'], {
          'LiveData': {
            id: "" + object.liveDataId
          }
        });
      }
      addedConnectableToSceneWithId(object.id);
    });

    projectObjects.filter(object => object.type == 'Powerstation').forEach(object => {
      let powerstation = createPlaceablePowerstation(new THREE.Vector2(object.x, object.y)).addToScene();
      finishPowerstation(powerstation, {
        'Connectable': {
          id: object.id
        },
        'Powerstation': {
          efficiency: object.efficiency
        }
      });
      if (object.hasOwnProperty('liveDataId')) {
        powerstation.addComponents(['LiveData'], {
          'LiveData': {
            id: "" + object.liveDataId
          }
        });
      }
      addedConnectableToSceneWithId(object.id);
    });

    projectObjects.filter(object => object.type == 'CableConnection').forEach(object => {
      createEntity(['Transform', 'CableConnection', 'Line', 'Selectable'], new THREE.Object3D(), {
        'CableConnection': {
          pointA: object.pointA,
          pointB: object.pointB
        }
      }).addToScene();
    });
  }

  /**
   * Returns the project objects ready to be uploaded
   */
  getProjectObjects() {
    var projectObjects = [];
    projectObjects = projectObjects.concat(entityManager.getAllComponentsData(['CableConnection']).map(entity => {
      return {
        type: 'CableConnection',
        pointA: entity.CableConnection.pointA,
        pointB: entity.CableConnection.pointB
      };
    }));

    projectObjects = projectObjects.concat(entityManager.getAllComponentsData(['Connectable', 'GridPosition', 'Windmill']).map(entity => {
      let obj = {
        type: 'Windmill',
        id: entity.Connectable.id,
        x: entity.GridPosition.position.x,
        y: entity.GridPosition.position.y,
        efficiency: entity.Windmill.efficiency
      };
      if (entityHasComponent(entity, 'LiveData')) {
        obj.liveDataId = getComponentDataForEntity('LiveData', entity).id;
      }
      return obj;
    }));

    projectObjects = projectObjects.concat(entityManager.getAllComponentsData(['Connectable', 'GridPosition', 'Powerstation']).map(entity => {
      let obj = {
        type: 'Powerstation',
        id: entity.Connectable.id,
        x: entity.GridPosition.position.x,
        y: entity.GridPosition.position.y,
        efficiency: entity.Powerstation.efficiency
      };
      if (entityHasComponent(entity, 'LiveData')) {
        obj.liveDataId = getComponentDataForEntity('LiveData', entity).id;
      }
      return obj;
    }));

    return projectObjects;
  }

  /**
   * Saves the project (the visualization related stuff)
   */
  save() {
    var terrainOptions = JSON.parse(JSON.stringify(this.terrainOptions));
    terrainOptions.restrictedAreas = this.terrain.getRestrictedAreas();
    delete terrainOptions.heightFactor;
    delete terrainOptions.pixelWidth;
    var projectObjects = {
      terrainOptions: terrainOptions,
      objects: this.getProjectObjects(),
      dirty: true
    };
    if (this.frontendVisualizationComponent) {
      this.frontendVisualizationComponent.saveProjectData(projectObjects);
    } else {
      console.log('No connection (projectSaveCallback) to frontend present, could not save. Logging project options to console');
      console.log(projectObjects);
    }
  }

  /**
   * Check whether a grid position is restricted according to the restricted areas and the bounding polygon
   */
  isGridPositionRestricted(gridPosition) {
    var worldPos = this.terrain.heightmap.gridCoordinatesToWorldCoordinates(gridPosition);

    var entities = entityManager.getAllComponentsData(['Transform', 'RestrictedAreaWithinDistance']);
    for (var i = 0; i < entities.length; i++) {
      let entity = entities[i];
      if (entity.RestrictedAreaWithinDistance.enabled && worldPos.distanceTo(entity.Transform.position) < entity.RestrictedAreaWithinDistance.distance) {
        return true;
      }
    }

    return this.terrain.isPointRestricted(worldPos);
  }

  /**
   * Check whether to connectables are already connected
   */
  areConnectablesAlreadyConnected(connectableId1, connectableId2) {
    var cables = entityManager.getAllComponentsData(['Line', 'CableConnection']);
    for (var i = 0; i < cables.length; i++) {
      // Very important! Use let instead of var to enable use of the loop variable objectWithMesh in asynchronous function (then callback) later
      let cable = cables[i];
      if (cable.CableConnection.pointA == Math.min(connectableId1, connectableId2) && cable.CableConnection.pointB == Math.max(connectableId1, connectableId2)) {
        return true;
      }
    }
    return false;
  }

  /**
   * This can be called from outside with live data concerning project objects. It will look for the entity with the
   * give live data id and apply the live data if suitable
   */
  liveDataIncoming(data) {
    if (globalSettings.liveDataEnabled) {
      for (var i = 0; i < data.length; i++) {
        var liveInfo = data[i];
        var idString = "" + liveInfo.id;
        var entities = entityManager.getAllComponentsData(['LiveData']).filter(el => el.LiveData.id == idString);

        if (entities.length > 1) {
          console.warn('More than one entity with live id ' + idString + ', nothing will be updated!');
        } else if (entities.length == 1) {
          let entity = entities[0];
          if (liveInfo.type == 'WindDistribution' && entityHasComponent(entity, 'WindDistribution')) {
            var componentData = getComponentDataForEntity('WindDistribution', entity);
            componentData.data = liveInfo.distribution;
          }
          if (liveInfo.type == 'Direction' && entityHasComponent(entity, 'Windmill')) {
            var rotatableChild = getEntityChildWithComponent(entity, 'RotateWithWindDirection');
            if (rotatableChild) {
              var componentData = getComponentDataForEntity('Transform', rotatableChild);
              componentData.rotation.z = liveInfo.direction;
            }
          }
          if (liveInfo.type == 'Speed' && entityHasComponent(entity, 'Windmill')) {
            var rotatableChild = getEntityChildWithComponent(entity, 'RotateWithWindspeed');
            if (rotatableChild) {
              var componentData = getComponentDataForEntity('RotateWithWindspeed', rotatableChild);
              componentData.speed = liveInfo.speed;
            }
          }
        }
      }
    }
  }

  /**
   * Assigns a live data id to an entity
   */
  setEntityLiveId(entity, liveId) {
    var entityId = parseEntityId(entity);

    var entities = entityManager.getAllComponentsData(['LiveData']).filter(el => el.LiveData.id == liveId && el.entityId != entityId);
    if (entities.length > 0) {
      alert('There already is an entity with live id ' + liveId);
    } else {
      if (entityHasComponent(entityId, 'LiveData')) {
        getComponentDataForEntity('LiveData', entityId).id = liveId;
      } else {
        addComponentsToEntity(entityId, ['LiveData'], {
          'LiveData': {
            id: liveId
          }
        });
      }
    }
  }

  /**
   * Removes the live data connection of an entity
   */
  removeEntityLiveDataConnection(entity) {
    var entityId = parseEntityId(entity);

    if (entityHasComponent(entityId, 'LiveData')) {
      removeComponentsFromEntity(['LiveData'], entityId);
    }
  }

  /**
   * Destroys the project and its objects (not working properly yet)
   */
  destroy() {
    // Not working properly yet, if this is called, the next loaded project is not shown correctly
    this.terrain.destroy();
    var entities = entityManager.getAllComponentsData(['Windmill']).concat(entityManager.getAllComponentsData(['Powerstation'])).concat(entityManager.getAllComponentsData(['CableConnection']));
    entities.map(entity => sceneGraphEntities[entity.entityId]).forEach(entity => entity.destroy());
  }
}
