var terrainGridFragmentShader = `
#define PHONG
uniform vec3 diffuse;
uniform vec3 emissive;
uniform vec3 specular;
uniform float shininess;
uniform float opacity;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 invProjModelViewMatrix;
uniform vec2 viewport;
uniform vec3 cameraPos;
uniform float gridSpacing;
uniform float gridLineWidth;
uniform bool gridVisible;
uniform bool gridInvisibleBelowZero;
varying float vRestricted;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_pars_fragment>
#include <gradientmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars>
#include <lights_phong_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
  #include <clipping_planes_fragment>
  vec4 diffuseColor = vec4( diffuse, opacity );
  ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
  vec3 totalEmissiveRadiance = emissive;
  #include <logdepthbuf_fragment>
  #include <map_fragment>
  // #include <color_fragment>
  #ifdef USE_COLOR
  if (vColor.r > 0.5 || vRestricted > 0.5) {
    diffuseColor.r = min(diffuseColor.r + 0.8, 1.0);
  }
  #endif
  #include <alphamap_fragment>
  #include <alphatest_fragment>
  #include <specularmap_fragment>
  #include <normal_fragment>
  #include <emissivemap_fragment>
  #include <lights_phong_fragment>
  #include <lights_template>
  #include <aomap_fragment>
  vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;
  #include <envmap_fragment>

  vec2 screen = vec2(gl_FragCoord.x / viewport.x * 2.0 - 1.0, gl_FragCoord.y / viewport.y * 2.0 - 1.0);

  float depth = gl_FragCoord.z;
  #ifdef USE_LOGDEPTHBUF
    #ifdef USE_LOGDEPTHBUF_EXT
      depth = gl_FragCoord.z;
    #else
      // Does not work for now, thus logarithmic depth buffer is disabled if the EXT_frag_depth extension is not available
    #endif
  #endif

  // https://stackoverflow.com/questions/38938498/how-do-i-convert-gl-fragcoord-to-a-world-space-point-in-a-fragment-shader/38938587#38938587
  vec4 ndc = vec4(
          (gl_FragCoord.x / viewport.x - 0.5) * 2.0,
          (gl_FragCoord.y / viewport.y - 0.5) * 2.0,
          (depth - 0.5) * 2.0,
          1.0);
  vec4 clip = invProjModelViewMatrix * ndc;
  vec3 worldpos = (clip / clip.w).xyz;

  gl_FragColor = vec4( outgoingLight, diffuseColor.a );

  if (gridVisible && !(gridInvisibleBelowZero && worldpos.z < 0.0) && length(cameraPos - worldpos) < 1500.0) {
    float gridSpacingLOD = gridSpacing;
    // if (length(cameraPos - worldpos) > 1000.0) {
    //   gridSpacingLOD = gridSpacingLOD * 10.0;
    // }
    float remainderX = mod(worldpos.x, gridSpacingLOD);
    float remainderY = mod(worldpos.y, gridSpacingLOD);
    if (remainderX < gridLineWidth/2.0 || remainderX > gridSpacingLOD - gridLineWidth/2.0 || remainderY < gridLineWidth/2.0 || remainderY > gridSpacingLOD - gridLineWidth/2.0) {
      gl_FragColor = vec4(0.15, 0.15, 0.15, 1.0);
    }
  }

  #include <tonemapping_fragment>
  #include <encodings_fragment>
  #include <fog_fragment>
  #include <premultiplied_alpha_fragment>
  #include <dithering_fragment>
}
`;
