// Global objects for the seaplane and the waterbox (black box under transparent seaplane)
var seaplane, waterbox;

/**
 * Creates a plane with the ocean texture.
 * Visualizes the sea level.
 */
function createWaterPlane(position, scale) {
  return new Promise((resolve, reject) => {
    var geometry = new THREE.PlaneBufferGeometry(scale.x, scale.y, 1, 1);

    loadTexture('images/ocean_texture.jpg')
      .catch(reject)
      .then(oceanTexture => {
        oceanTexture.wrapS = oceanTexture.wrapT = THREE.RepeatWrapping;
        textureScale = scale.x / 100.0 / 5.0;
        oceanTexture.repeat.set(textureScale / scale.y * scale.x, textureScale);

        var material = new THREE.MeshPhongMaterial({
          map: oceanTexture,
          transparent: true,
          opacity: 0.5,
          depthWrite: false,
          onBeforeCompile: (shader) => {
            // These shaders draw the grid on the ocean plane
            shader.fragmentShader = terrainGridFragmentShader;
            shader.vertexShader = terrainGridVertexShader;

            // This uniform is false here to show grid lines everywhere. It's true for the terrain, where the lines
            // should not be shown underwater (because they are already shown here on the surface)
            shader.uniforms.gridInvisibleBelowZero = new THREE.Uniform(false);
          }
        });

        var mesh = new THREE.Mesh(geometry, material);
        mesh.position.copy(position);
        mesh.receiveShadow = true;

        resolve(mesh);
      });
  });
}

/**
 * Creates a plane with a color at a certain position/scale/rotation
 */
function createSimplePlane(position, scale, rotation, color) {
  var geometry = new THREE.PlaneGeometry(scale.x, scale.y, 1);
  var material = new THREE.MeshBasicMaterial({
    color: 0,
    fog: false
  });
  var plane = new THREE.Mesh(geometry, material);
  plane.position.copy(position);
  plane.rotation.copy(rotation);
  return plane;
}

/**
 * Creates the waterbox (black box without plane on top, with faces facing inside), which ensures that we cannot see the skybox through the transparent ocean plane 
 * where there is no terrain (e.g. in the distance)
 */
function createWaterbox(scale, depth) {
  var box = new THREE.Object3D();
  var color = 0x000000;
  box.add(createSimplePlane(new THREE.Vector3(0, 0, -depth), new THREE.Vector3(scale, scale, 1), new THREE.Euler(), color));
  box.add(createSimplePlane(new THREE.Vector3(0, scale / 2.0, -depth / 2.0), new THREE.Vector3(scale, depth, 1), new THREE.Euler(Math.PI / 2.0, 0, 0), color));
  box.add(createSimplePlane(new THREE.Vector3(0, -scale / 2.0, -depth / 2.0), new THREE.Vector3(scale, depth, 1), new THREE.Euler(-Math.PI / 2.0, 0, 0), color));
  box.add(createSimplePlane(new THREE.Vector3(scale / 2.0, 0, -depth / 2.0), new THREE.Vector3(depth, scale, 1), new THREE.Euler(0, -Math.PI / 2.0, 0), color));
  box.add(createSimplePlane(new THREE.Vector3(-scale / 2.0, 0, -depth / 2.0), new THREE.Vector3(depth, scale, 1), new THREE.Euler(0, Math.PI / 2.0, 0), color));
  return box;
}

/**
 * Creates the ocean, consisting of the water plane and the water box, both move with the camera, ensuring they are always
 * visible, no matter where the camera is moved
 */
function createOcean() {
  var groundScale = 100000.0;
  createWaterPlane(new THREE.Vector3(0, 0, -0.1), new THREE.Vector3(groundScale, groundScale, 1))
    .catch(console.err)
    .then(mesh => {
      seaplane = createEntity(['Transform', 'TextureAnimation', 'MoveTextureWithWind', 'ReceiveShadow', 'MoveWithCamera'], mesh, {
        'TextureAnimation': {
          delta: new THREE.Vector2(0.00001 / 5.0, 0.00001 / 5.0)
        }
      }).addToScene();
    });

  waterbox = createEntity(['Transform', 'MoveWithCamera'], createWaterbox(groundScale, 50), {}).addToScene();
}

// function createLine(start, end, color) {
//   var geometry = new THREE.Geometry();
//   var material = new THREE.LineBasicMaterial({
//     color: color
//   });
//   geometry.vertices.push(new THREE.Vector3(start.x, start.y, start.z));
//   geometry.vertices.push(new THREE.Vector3(end.x, end.y, end.z));
//   var line = new THREE.Line(geometry, material);
//   return Promise.resolve(line);
// }

// /*
//  * creates the grid out of single lines 
//  */
// function createGrid2() {
//   // grid = new THREE.Object3D();
//   grid = createEntity(['Transform'], new THREE.Object3D());
//   scene.add(grid);

//   for (var yi = 0; yi < project.terrain.heightmap.gridSize.y; yi++) {
//     createLine(project.terrain.heightmap.gridCoordinatesToWorldCoordinates(new THREE.Vector2(0, yi)).setZ(0), project.terrain.heightmap.gridCoordinatesToWorldCoordinates(new THREE.Vector2(project.terrain.heightmap.gridSize.x - 1, yi)).setZ(0), 0x000000).then((line) => {
//       grid.add(line);
//     });
//   }
//   for (var xi = 0; xi < project.terrain.heightmap.gridSize.x; xi++) {
//     createLine(project.terrain.heightmap.gridCoordinatesToWorldCoordinates(new THREE.Vector2(xi, 0)).setZ(0), project.terrain.heightmap.gridCoordinatesToWorldCoordinates(new THREE.Vector2(xi, project.terrain.heightmap.gridSize.y - 1)).setZ(0), 0x000000).then((line) => {
//       grid.add(line);
//     });
//   }

//   grid.visible = globalSettings.gridVisible;
// }

// function createGrid() {
//   grid = createEntity(['Transform'], new THREE.Object3D(), {
//     'Transform': {
//       position: new THREE.Vector3(0, 0, 50),
//       rotation: new THREE.Euler(Math.PI / 2.0)
//     }
//   });
//   scene.add(grid);
//   return;

//   var positions = [];
//   var offsets = [];
//   var scales = [];

//   var triangles = 1;
//   // positions.push(0.025, -0.025, 0);
//   // positions.push(-0.025, 0.025, 0);
//   // positions.push(0, 0, 0.025);
//   positions.push(0.0, 0.0, -100.0);
//   positions.push(100.0, 0.0, -100.0);
//   positions.push(100.0, 0.0, 100.0);

//   var instances = 1;
//   for (var i = 0; i < instances; i++) {
//     offsets.push(0, 0, 0);
//     scales.push(1, 1, 1);
//   }

//   var geometry = new THREE.InstancedBufferGeometry();
//   geometry.maxInstancedCount = instances; // set so its initalized for dat.GUI, will be set in first draw otherwise
//   geometry.addAttribute('position', new THREE.Float32BufferAttribute(positions, 3));
//   // geometry.addAttribute('offset', new THREE.InstancedBufferAttribute(new Float32Array(offsets), 3));
//   // geometry.addAttribute('color', new THREE.InstancedBufferAttribute(new Float32Array(colors), 4));
//   // geometry.addAttribute('orientationStart', new THREE.InstancedBufferAttribute(new Float32Array(orientationsStart), 4));
//   // geometry.addAttribute('orientationEnd', new THREE.InstancedBufferAttribute(new Float32Array(orientationsEnd), 4));

//   var geometry2 = new THREE.PlaneGeometry(5, 20);

//   var shader = THREE.ShaderLib['phong'];

//   var material;

//   // material = new THREE.RawShaderMaterial({
//   //   // uniforms: {},
//   //   uniforms: THREE.UniformsUtils.merge([
//   //     THREE.UniformsLib.common,
//   //     THREE.UniformsLib.specularmap,
//   //     THREE.UniformsLib.envmap,
//   //     THREE.UniformsLib.aomap,
//   //     THREE.UniformsLib.lightmap,
//   //     THREE.UniformsLib.emissivemap,
//   //     THREE.UniformsLib.bumpmap,
//   //     THREE.UniformsLib.normalmap,
//   //     THREE.UniformsLib.displacementmap,
//   //     THREE.UniformsLib.gradientmap,
//   //     THREE.UniformsLib.fog,
//   //     THREE.UniformsLib.lights, {
//   //       emissive: {
//   //         value: new THREE.Color(0x000000)
//   //       },
//   //       specular: {
//   //         value: new THREE.Color(0x111111)
//   //       },
//   //       shininess: {
//   //         value: 30
//   //       }
//   //     }
//   //   ]),
//   //   vertexShader: gridLineVertexShader,
//   //   fragmentShader: gridLineFragmentShader,
//   //   side: THREE.DoubleSide,
//   //   // depthTest: true,
//   //   // depthWrite: true,
//   //   // transparent: false,
//   // });
//   // console.log(material);

//   material = new THREE.ShaderMaterial(THREE.ShaderLib['phong']);

//   material = new THREE.MeshPhongMaterial({
//     side: THREE.DoubleSide,
//     color: new THREE.Color(1, 0, 0),
//     onBeforeCompile: function(shader) {
//       shader.fragmentShader = gridLineFragmentShader;
//       shader.vertexShader = gridLineVertexShader;
//       console.log(shader);
//     }
//   });
//   console.log(material);

//   var mesh = new THREE.Mesh(geometry2, material);
//   grid.add(mesh);

//   if (renderer.extensions.get('ANGLE_instanced_arrays') === false) {
//     console.err('Instancing not supported');
//   }

//   // grid.visible = gridvisible;
//   grid.renderOrder = 100;
//   grid.children[0].renderOrder = 100;
// }
