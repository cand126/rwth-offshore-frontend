// Contains the name of the currently active mode
var currentEditMode = 'mode_default';

// Pointer to the toolbar in the frontend, used to switch modes there
var frontendToolbar = null;

// Dictionary of all editmodes, filled below
var editModes;

/**
 * Convenience minmax function
 */
function minmax(val, min, max) {
  return Math.max(min, Math.min(val, max));
}

/**
 * Intersects the ray from the mouse position into the scene with all entities with the Selectable component.
 * The first intersection is added to the outline pass so that it is perceivably hovered
 */
function hoverObjects(component) {
  let selectableSceneGraphObjects = entityManager.getAllComponentsData([component]).map(obj => sceneGraphEntities[obj.entityId]);

  raycaster.setFromCamera(mouse.pos, camera);
  var intersects = raycaster.intersectObjects(selectableSceneGraphObjects, true);

  outlinePass.selectedObjects = [];

  if (intersects.length > 0) {
    let parentWithComponent = findParentWithComponent(intersects[0].object, component);
    if (parentWithComponent) {
      outlinePass.selectedObjects = [parentWithComponent];
    }
  }
}

/**
 * Abstract class for an editing mode. 
 */
class EditingMode {
  constructor(name, gridHighlightElementVisible) {
    this.name = name;
    this.gridHighlightElementVisible = gridHighlightElementVisible;
  }

  initMode() {
    if (project && project.terrain && project.terrain.gridHighlightElement) {
      project.terrain.gridHighlightElement.visible = this.gridHighlightElementVisible;
    }
  }
  exitMode() {}
  resetMode() {
    this.exitMode();
    this.initMode();
  }

  mouseDown(mouse) {}
  mouseDoubleclickDown(mouse) {}
  mouseUp(mouse) {}
  mouseMove(mouse) {}
  mouseScrolled(mouse) {}

  touchDown(mouse) {}
  touchDoubleclickDown(mouse) {}
  touchUp(mouse) {}
  touchMove(mouse) {}
}

/**
 * The default camera mode. Objects can be selected, therefore they are hovered on mouse move and selected on mouse click.
 * A double click invokes a camera move to the clicked grid position
 */
class ViewMode extends EditingMode {
  mouseUp(mouse) {
    if (!mouse.dragged) {
      if (mouse.gridPos && outlinePass.selectedObjects.length > 0 && outlinePass.selectedObjects[0].hasComponent('Selectable')) { // A selectable object is clicked
        // Remove prior selections
        entityManager.getAllComponentsData(['Selectable']).forEach(selectableObject => selectableObject.Selectable.selected = false);

        outlinePass.selectedObjects[0].getComponentData('Selectable').selected = true;

        // Highlight selected object differently
        selectedObjectOutlinePass.selectedObjects = [outlinePass.selectedObjects[0]];
      } else { // Nothing is clicked
        entityManager.getAllComponentsData(['Selectable']).forEach(selectableObject => selectableObject.Selectable.selected = false);
        selectedObjectOutlinePass.selectedObjects = [];
      }
    }
  }

  touchUp(mouse) {
    hoverObjects('Selectable');
    this.mouseUp(mouse);
  }

  mouseDoubleclickDown(mouse) {
    if (mouse.gridPos) {
      // Camera placement is allowed within a margin around grid
      var cameraAllowedGridMargin = 20;
      setCamera(new THREE.Vector2(minmax(mouse.gridPos.x, -cameraAllowedGridMargin, project.terrain.heightmap.gridSize.x + cameraAllowedGridMargin), minmax(mouse.gridPos.y, -cameraAllowedGridMargin, project.terrain.heightmap.gridSize.y + cameraAllowedGridMargin)));
    }
  }

  touchDoubleclickDown(mouse) {
    this.mouseDoubleclickDown(mouse);
  }

  mouseMove(mouse) {
    hoverObjects('Selectable');
  }
}

/**
 * Move mode, Movable objects are hovered on mouse move, picked up on mouse down and placed again on mouse up
 */
class MoveMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.movingObject = null;
    this.dirty = false;
  }

  exitMode() {
    if (this.movingObject && this.movingObject.hasComponent('RestrictedAreaWithinDistance')) {
      this.movingObject.getComponentData('RestrictedAreaWithinDistance').enabled = true;
    }
    this.movingObject = null;
    canvas.style.cursor = 'auto';
    this.dirty = false;
  }

  mouseDown(mouse) {
    if (outlinePass.selectedObjects.length > 0) {
      this.movingObject = outlinePass.selectedObjects[0];
      if (this.movingObject.hasComponent('RestrictedAreaWithinDistance')) {
        this.movingObject.getComponentData('RestrictedAreaWithinDistance').enabled = false;
      }
      controls.disable();
    }
  }

  touchDown(mouse) {
    hoverObjects('Selectable');
    if (outlinePass.selectedObjects.length > 0) {
      this.movingObject = outlinePass.selectedObjects[0];
      if (this.movingObject.hasComponent('RestrictedAreaWithinDistance')) {
        this.movingObject.getComponentData('RestrictedAreaWithinDistance').enabled = false;
      }
      controls.disable();
    }
  }

  mouseMove(mouse) {
    if (mouse.clicked && this.movingObject) {
      if (mouse.gridPos && !project.isGridPositionRestricted(mouse.gridPos)) {
        getComponentDataForEntity('GridPosition', this.movingObject.entityId).position.copy(mouse.gridPos);
        this.dirty = true;
      }
    } else {
      hoverObjects('Movable');
      if (outlinePass.selectedObjects.length > 0) {
        canvas.style.cursor = 'move';
      } else {
        canvas.style.cursor = 'auto';
      }
    }
  }

  touchMove(mouse) {
    if (this.movingObject && mouse.gridPos && !project.isGridPositionRestricted(mouse.gridPos)) {
      getComponentDataForEntity('GridPosition', this.movingObject.entityId).position.copy(mouse.gridPos);
      this.dirty = true;
    }
  }

  mouseUp(mouse) {
    if (this.dirty) {
      project.save();
      this.dirty = false;
    }
    controls.enable();
    this.resetMode();
  }

  touchUp(mouse) {
    if (this.dirty) {
      project.save();
      this.dirty = false;
    }
    controls.enable();
    this.resetMode();
  }
}

/**
 * Create mode for windmills. If a windmill can be placed, it is on mouse up
 */
class CreateWindmillMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.entityToBeCreated = null;
    this.touchDownGridPos = null;
  }

  initMode() {
    super.initMode();
    this.touchDownGridPos = null;
    this.entityToBeCreated = createPlaceableWindmill(new THREE.Vector3()).addToScene();
    addComponentsToEntity(this.entityToBeCreated, ['GridPositionByMousePosition'], {});
    controls.disable();
    canvas.style.cursor = 'cell';
  }

  exitMode() {
    if (this.entityToBeCreated) {
      this.entityToBeCreated.destroy();
      this.entityToBeCreated = null;
    }
    this.touchDownGridPos = null;
    controls.enable();
    canvas.style.cursor = 'auto';
  }

  placeWindmill(mouse) {
    if (!project.isGridPositionRestricted(mouse.gridPos)) {
      removeComponentsFromEntity(['GridPositionByMousePosition'], this.entityToBeCreated);
      finishWindmill(this.entityToBeCreated);
      project.save();
      this.entityToBeCreated = null;
      this.resetMode();
    }
  }

  mouseUp(mouse) {
    this.placeWindmill(mouse);
  }

  touchUp(mouse) {
    if (this.touchDownGridPos && mouse.gridPos.x == this.touchDownGridPos.x && mouse.gridPos.y == this.touchDownGridPos.y) {
      this.placeWindmill(mouse);
    }
    this.touchDownGridPos = mouse.gridPos.clone();
  }
}

/**
 * Create mode for powerstations. If a powerstation can be placed, it is on mouse up
 */
class CreatePowerstationMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.entityToBeCreated = null;
    this.touchDownGridPos = null;
  }

  initMode() {
    super.initMode();
    this.touchDownGridPos = null;
    this.entityToBeCreated = createPlaceablePowerstation(new THREE.Vector3()).addToScene();
    addComponentsToEntity(this.entityToBeCreated, ['GridPositionByMousePosition'], {});
    controls.disable();
    canvas.style.cursor = 'cell';
  }

  exitMode() {
    if (this.entityToBeCreated) {
      this.entityToBeCreated.destroy();
      this.entityToBeCreated = null;
    }
    this.touchDownGridPos = null;
    controls.enable();
    canvas.style.cursor = 'auto';
  }

  placePowerstation() {
    if (!project.isGridPositionRestricted(mouse.gridPos)) {
      removeComponentsFromEntity(['GridPositionByMousePosition'], this.entityToBeCreated);
      finishPowerstation(this.entityToBeCreated);
      project.save();
      this.entityToBeCreated = null;
      this.resetMode();
    }
  }

  mouseUp(mouse) {
    this.placePowerstation(mouse);
  }

  touchUp(mouse) {
    if (this.touchDownGridPos && mouse.gridPos.x == this.touchDownGridPos.x && mouse.gridPos.y == this.touchDownGridPos.y) {
      this.placePowerstation(mouse);
    }
    this.touchDownGridPos = mouse.gridPos.clone();
  }
}

/**
 * Wiring mode. Cable starting points can be set with mouse down on a Connectable entity. The cable endpoint
 * follows the mouse on mouse move and snaps to Connectable endpoints. The final cable is created on mouse up.
 */
class CreateWiringMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.tempWire = null;
  }

  initMode() {
    super.initMode();
    this.tempWire = null;
    controls.disable();
  }

  exitMode() {
    if (this.tempWire) {
      this.tempWire.destroy();
      this.tempWire = null;
    }
    controls.enable();
    canvas.style.cursor = 'auto';
  }

  mouseDown(mouse) {
    if (outlinePass.selectedObjects.length > 0 && outlinePass.selectedObjects[0].hasComponent('Connectable')) {
      this.tempWire = createEntity(['Transform', 'Line'], new THREE.Object3D(), {
        'Line': {
          pointA: outlinePass.selectedObjects[0].position.clone(),
          pointB: outlinePass.selectedObjects[0].position.clone()
        }
      }).addToScene();
      this.tempWire.pointAId = outlinePass.selectedObjects[0].getComponentData('Connectable').id;
    }
  }

  touchDown(mouse) {
    hoverObjects('Connectable');
    this.mouseDown(mouse);
  }

  mouseMove(mouse) {
    hoverObjects('Connectable');
    if (mouse.clicked && mouse.gridPos && this.tempWire) {
      if (outlinePass.selectedObjects.length > 0 && outlinePass.selectedObjects[0].getComponentData('Connectable').id != this.tempWire.pointAId) {
        // In case we move over a connectable object, the wire snaps to it
        getComponentDataForEntity('Line', this.tempWire.entityId).pointB.copy(outlinePass.selectedObjects[0].getComponentData('Transform').position);
        canvas.style.cursor = 'alias';
      } else {
        // In case we move somewhere else on the grid, the temporary cable is shown connected to the hovered grid cell
        var pointOnGround = project.terrain.heightmap.gridCoordinatesToWorldCoordinates(mouse.gridPos).clone().setZ(Math.max(0.4, project.terrain.heightmap.getGridHeight(mouse.gridPos)));
        getComponentDataForEntity('Line', this.tempWire.entityId).pointB.copy(pointOnGround);
        canvas.style.cursor = 'no-drop';
      }
    } else {
      if (outlinePass.selectedObjects.length > 0) {
        canvas.style.cursor = 'crosshair';
      } else {
        canvas.style.cursor = 'auto';
      }
    }
  }

  touchMove(mouse) {
    this.mouseMove(mouse);
  }

  mouseUp(mouse) {
    // If we stop dragging our wiring on a second selectable object, we create the connection

    if (outlinePass.selectedObjects.length > 0) {
      if (!outlinePass.selectedObjects[0].hasComponent) {
        console.error("Object has no hasComponent function!");
        console.error(outlinePass.selectedObjects[0]);
      }
    }
    if (this.tempWire) {
      if (mouse.gridPos && outlinePass.selectedObjects.length > 0 && outlinePass.selectedObjects[0].hasComponent('Connectable')) {
        var pointBId = outlinePass.selectedObjects[0].getComponentData('Connectable').id;
        if (pointBId == -1) {
          console.error("Attention: Connectable id is not set! Cannot create wiring");
        } else {
          // Check if we selected the same connectable twice
          if (this.tempWire.pointAId != pointBId) {
            // Check if the connectables are already connected
            if (project.areConnectablesAlreadyConnected(this.tempWire.pointAId, pointBId)) {
              // console.error("Connectables are already connected");
            } else {
              var newWire = createEntity(['Transform', 'CableConnection', 'Line', 'Selectable'], new THREE.Object3D(), {
                'CableConnection': {
                  pointA: Math.min(this.tempWire.pointAId, pointBId), // Always store lower id first
                  pointB: Math.max(this.tempWire.pointAId, pointBId)
                }
              }).addToScene();
              project.save();
            }
          }
        }
      }
      this.resetMode();
    }
  }

  touchUp(mouse) {
    this.mouseUp(mouse);
  }
}

/**
 * Remove mode. Remove cables and objects on mouse up
 */
class RemoveMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.touchEntitySelected = null;
  }

  initMode() {
    this.touchEntitySelected = null;
  }

  exitMode() {
    this.touchEntitySelected = null;
    canvas.style.cursor = 'auto';
  }

  mouseDown(mouse) {
    controls.disable();
  }

  touchDown(mouse) {
    hoverObjects('Selectable');
    this.mouseDown(mouse);
  }

  mouseMove(mouse) {
    hoverObjects('Selectable');
    if (outlinePass.selectedObjects.length > 0) {
      canvas.style.cursor = 'no-drop';
    } else {
      canvas.style.cursor = 'auto';
    }
  }

  touchMove(mouse) {
    hoverObjects('Selectable');
  }

  mouseUp(mouse) {
    if (outlinePass.selectedObjects.length > 0) {
      var entity = outlinePass.selectedObjects[0];
      entity.destroy();
      project.save();
    }
    controls.enable();
  }

  touchUp(mouse) {
    if (outlinePass.selectedObjects.length > 0 && this.touchEntitySelected == outlinePass.selectedObjects[0]) {
      this.mouseUp(mouse);
    }
    if (outlinePass.selectedObjects.length > 0) {
      this.touchEntitySelected = outlinePass.selectedObjects[0];
    }
  }
}

/**
 * Restricted polygon area creation mode. Click to add polygon corner points.
 * The polygon is added to the restricted areas on connecting the last with the first
 * point. 
 */
class RestrictedAreaMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.tempWire = null;
    this.wires = [];
    this.polygon = [];
  }

  initMode() {
    super.initMode();
    this.tempWire = null;
    this.polygon = [];
    canvas.style.cursor = 'crosshair';
    controls.disable();
  }

  exitMode() {
    if (this.tempWire) {
      this.tempWire.destroy();
      this.tempWire = null;
    }
    if (this.wires.length > 0) {
      this.wires.forEach(wire => {
        wire.destroy();
      });
      this.wires = [];
    }
    this.polygon = [];
    controls.enable();
    canvas.style.cursor = 'auto';
  }

  moveCurrentWireEndpoint(mouse) {
    if (mouse.gridPos && this.tempWire) {
      var pointB = project.terrain.heightmap.gridCoordinatesToWorldCoordinates(mouse.gridPos);
      getComponentDataForEntity('Line', this.tempWire.entityId).pointB.copy(pointB);

      if (this.polygon.length >= 3 && mouse.gridPos.x == this.polygon[0].x && mouse.gridPos.y == this.polygon[0].y) {
        canvas.style.cursor = 'alias';
      }
    }
  }

  mouseMove(mouse) {
    this.moveCurrentWireEndpoint(mouse);
  }

  mouseUp(mouse) {
    if (mouse.gridPos) {
      if (this.polygon.length >= 3 && mouse.gridPos.x == this.polygon[0].x && mouse.gridPos.y == this.polygon[0].y) {
        // Polygon was closed!
        project.terrain.addRestrictedPolygonFromGridPositions(this.polygon);
        project.save();
        this.resetMode();
      } else {
        if (this.tempWire) {
          this.wires.push(this.tempWire);
        }

        this.tempWire = createEntity(['Transform', 'Line', 'Material'], new THREE.Object3D(), {
          'Line': {
            pointA: project.terrain.heightmap.gridCoordinatesToWorldCoordinates(mouse.gridPos), // Begin line at mouse.pos, pointB gets changed later on mouse move
            pointB: project.terrain.heightmap.gridCoordinatesToWorldCoordinates(mouse.gridPos)
          },
          'Material': {
            color: new THREE.Color(0xff0000)
          }
        }).addToScene();

        this.polygon.push(mouse.gridPos);
      }
    }
  }

  touchDown(mouse) {
    this.moveCurrentWireEndpoint(mouse);
  }

  touchMove(mouse) {
    this.moveCurrentWireEndpoint(mouse);
  }

  touchUp(mouse) {
    this.mouseUp(mouse);
  }
}

/**
 * Restricted circle creation mode. Use the mouse wheel to increase circle size. The restricted
 * area circle is created on mouse up
 */
class RestrictedAreaCircleMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
    this.touchDownGridPos = null;
  }

  initMode() {
    super.initMode();
    this.touchDownGridPos = null;
    canvas.style.cursor = 'crosshair';
    controls.disable();

    this.radius = project.terrain.heightmap.gridSpacing / 4.0;

    ////////// circle element ////////////
    this.circleElement = createEntity(['Transform', 'GridPosition', 'GridPositionByMousePosition', 'Circle', 'Material'], new THREE.Object3D(), {
      'Transform': {
        scale: new THREE.Vector3(this.radius, this.radius, 1),
        position: new THREE.Vector3(0, 0, 0.5)
      },
      'GridPosition': {
        heightMode: 'untouched'
      },
      'Material': {
        color: new THREE.Color(1, 0, 0),
        transparent: true,
        opacity: 0.5
      },
      'GridPositionByMousePosition': {
        lockOnHoveredObjects: false
      }
    }).addToScene();
  }

  exitMode() {
    if (this.circleElement) {
      this.circleElement.destroy();
      this.circleElement = null;
    }
    this.touchDownGridPos = null;
    controls.enable();
    canvas.style.cursor = 'auto';
  }

  // mouseScrolled(mouse) {
  //   var scrollStep = project.terrain.heightmap.gridSpacing / 2.0 / 10.0;
  //   if (mouse.lastScrolledUp) {
  //     this.radius += scrollStep * 2.0;
  //   } else {
  //     this.radius = Math.max(project.terrain.heightmap.gridSpacing / 2.0, this.radius - scrollStep);
  //   }
  //   this.circleElement.getComponentData('Transform').scale.set(this.radius, this.radius, 1);
  // }

  mouseDown(mouse) {
    if (mouse.gridPos) {
      this.touchDownGridPos = mouse.gridPos.clone();
      if (this.circleElement.hasComponent('GridPositionByMousePosition')) {
        removeComponentsFromEntity(['GridPositionByMousePosition'], this.circleElement);
      }
    }
  }

  touchDown(mouse) {
    if (mouse.gridPos) {
      this.touchDownGridPos = mouse.gridPos.clone();
    }
  }

  updateRadius() {
    if (this.touchDownGridPos && mouse.gridPos && mouse.worldPos) {
      this.radius = project.terrain.heightmap.gridCoordinatesToWorldCoordinates(this.touchDownGridPos).setZ(0).distanceTo(mouse.worldPos.clone().setZ(0));
      this.radius = Math.max(this.radius, project.terrain.heightmap.gridSpacing / 2.0);
      this.circleElement.getComponentData('Transform').scale.set(this.radius, this.radius, 1);
    }
  }

  mouseMove(mouse) {
    this.updateRadius();
  }

  touchMove(mouse) {
    if (this.circleElement.hasComponent('GridPositionByMousePosition')) {
      removeComponentsFromEntity(['GridPositionByMousePosition'], this.circleElement);
    }
    this.updateRadius();
  }

  createCircle(mouse) {
    var center = this.circleElement.getComponentData('GridPosition').position;
    project.terrain.addRestrictedCircleFromGridPosition(center, this.radius);
    project.save();
    this.resetMode();
  }

  mouseUp(mouse) {
    if (this.circleElement && this.radius > 0) {
      this.createCircle(mouse);
    }
  }

  touchUp(mouse) {
    if (this.circleElement && this.radius > 0) {
      this.createCircle(mouse);
    }
  }
}

/**
 * Restricted area remove mode. Restricted areas are removed on mouse up if the clicked grid
 * position lies within it.
 */
class RestrictedAreaRemoveMode extends EditingMode {
  constructor(name, gridHighlightElementVisible) {
    super(name, gridHighlightElementVisible);
  }

  initMode() {
    super.initMode();
    canvas.style.cursor = 'no-drop';
    controls.disable();
  }

  exitMode() {
    controls.enable();
    canvas.style.cursor = 'auto';
  }

  mouseUp(mouse) {
    project.terrain.removeRestrictedAreasAtGridPosition(mouse.gridPos);
    project.save();
  }

  touchUp(mouse) {
    this.mouseUp(mouse);
  }
}

editModes = {
  'mode_default': new ViewMode('mode_default', false),
  'mode_move': new MoveMode('mode_move', true),
  'mode_createWindmill': new CreateWindmillMode('mode_createWindmill', true),
  'mode_createPowerstation': new CreatePowerstationMode('mode_createPowerstation', true),
  'mode_createWiring': new CreateWiringMode('mode_createWiring', false),
  'mode_remove': new RemoveMode('mode_remove', false),
  'mode_restrictArea': new RestrictedAreaMode('mode_restrictArea', false),
  'mode_restrictAreaCircle': new RestrictedAreaCircleMode('mode_restrictAreaCircle', false),
  'mode_removeRestrictedArea': new RestrictedAreaRemoveMode('mode_removeRestrictedArea', false)
};

/**
 * current editing mode getter
 */
function getCurrentEditMode() {
  return editModes[currentEditMode];
}

/**
 * Switch mode by calling the exit method of the old and the init method of the new mode.
 */
function switchMode(mode, DontUpdateToolbar) {
  getCurrentEditMode().exitMode();
  currentEditMode = mode;
  getCurrentEditMode().initMode();

  if (frontendToolbar && !DontUpdateToolbar) {
    frontendToolbar.modeHasChanged(mode);
  }
}
