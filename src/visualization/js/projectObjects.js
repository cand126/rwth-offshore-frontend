// Global wind speed, gets overridden by live data for single windmills
var windspeed = 4.0;

// windspeed characteristic curve points
const wscp = [
  [2.0, 6.0],
  [11.25, 15.0],
  [13.0, 19.0],
  [14.0, 19.5]
];

// Global wind direction (angle to north), gets overridden by live data for single windmills
var windDirection = 0;

// Holds current highest connectable id, the next one will be currentConnectableId+1
var currentConnectableId = 0;

/**
 * Returns a new unused connectable id
 */
function getNewConnectableId() {
  return currentConnectableId++;
}

/**
 * Updates the highest connectable id given an id was added from loading an object with a fixed id
 */
function addedConnectableToSceneWithId(id) {
  currentConnectableId = Math.max(currentConnectableId, id + 1);
}

function linearInterpolation(x0, y0, x1, y1, x) {
  return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
}

/**
 * Implements a characteristic curve function from points wscp
 */
function windspeedToRPM(u) {
  if (u < 2.0) {
    return 0.0;
  } else if (u < 14.0) {
    for (var i = 1; i < wscp.length; i++) {
      if (u < wscp[i][0]) {
        return linearInterpolation(wscp[i - 1][0], wscp[i - 1][1], wscp[i][0], wscp[i][1], u);
      }
    }
    return linearInterpolation(2.0, 11.25, u);
  } else {
    return 19.5;
  }
}

/**
 * Creates an intermediate windmill that is placed in the respective editing mode, has to be finished with finishWindmill
 */
function createPlaceableWindmill(position) {
  // Create windmill entity at "position" with tower mesh attached
  var windmillEntity = createEntity(['Transform', 'GridPosition', 'CastShadow', 'Selectable', 'WindDistribution'], new THREE.Object3D(), {
    'GridPosition': {
      position: new THREE.Vector2(position.x, position.y),
      heightMode: 'elevateOnLand'
    }
  });

  var towerFootEntity = createEntity(['Transform', 'Box', 'Material', 'ExtendToGround'], new THREE.Object3D(), {
    'Transform': {
      scale: new THREE.Vector3(10, 10, 10)
    },
    'Material': {
      color: new THREE.Color(0xffffff)
    }
  }).addToObject(windmillEntity);

  // Create group for windmill wings with center at tower head
  var rotatableGroupEntity = createEntity(['Transform', 'RotateWithWindDirection', 'Mesh'], new THREE.Object3D(), {
    'Mesh': {
      filenameMesh: 'models/windmill_tower.obj',
      filenameMaterial: 'models/windmill_tower.mtl'
    }
  }).addToObject(windmillEntity);

  // Create group for windmill wings with center at tower head
  var wingGroupEntity = createEntity(['Transform', 'TransformAnimation', 'RotateWithWindspeed', 'RotateWithGlobalWindspeed'], new THREE.Object3D(), {
    'Transform': {
      position: new THREE.Vector3(1.26589, -6.0772, 64.9954)
    }
  }).addToObject(rotatableGroupEntity);

  let initialWingPosition = Math.random() * 2.0 * Math.PI;
  let numberOfWings = 3;
  for (var i = 0; i < numberOfWings; i++) {
    createEntity(['Transform', 'Mesh'], new THREE.Object3D(), {
      'Transform': {
        rotation: new THREE.Euler(0, (initialWingPosition + i * 360.0 / numberOfWings / 180.0 * Math.PI) % 360.0, 0)
      },
      'Mesh': {
        filenameMesh: 'models/wings.obj',
        filenameMaterial: 'models/wings.mtl'
      }
    }).addToObject(wingGroupEntity);
  }
  return windmillEntity;
}

/**
 * Finishes an intermediate windmill by adding missing components
 */
function finishWindmill(windmill, componentData) {
  var newComponentData = {
    'Connectable': {
      id: getNewConnectableId()
    },
    'Windmill': {
      efficiency: 0.5
    },
    'RestrictedAreaWithinDistance': {
      distance: 100
    }
  };
  if (componentData) {
    Object.keys(componentData).forEach(function(key) {
      newComponentData[key] = componentData[key];
    });
  }

  addComponentsToEntity(windmill, ['Movable', 'Connectable', 'Windmill', 'RestrictedAreaWithinDistance'], newComponentData);
}

/**
 * Creates an intermediate power station that is placed in the respective editing mode, has to be finished with finishPowerstation
 */
function createPlaceablePowerstation(position) {
  var powerstationEntity = createEntity(['Transform', 'GridPosition', 'CastShadow', 'Selectable'], new THREE.Object3D(), {
    'GridPosition': {
      position: new THREE.Vector2(position.x, position.y),
      heightMode: 'elevateOnLand'
    }
  });

  createEntity(['Transform', 'Mesh'], new THREE.Object3D(), {
    'Transform': {
      scale: new THREE.Vector3(2, 2, 2),
      rotation: new THREE.Euler(0, 0, Math.PI),
    },
    'Mesh': {
      filenameMesh: 'models/etowerdecimated_thicker.obj',
      filenameMaterial: 'models/etowerdecimated_thicker.mtl'
    }
  }).addToObject(powerstationEntity);

  var powerstationFootEntity = createEntity(['Transform', 'Box', 'Material', 'ExtendToGround'], new THREE.Object3D(), {
    'Transform': {
      scale: new THREE.Vector3(10, 10, 10)
    },
    'Material': {
      color: new THREE.Color(0xffffff)
    }
  }).addToObject(powerstationEntity);
  return powerstationEntity;
}

/**
 * Finishes an intermediate windmill by adding missing components
 */
function finishPowerstation(powerstation, componentData) {
  var newComponentData = {
    'Connectable': {
      id: getNewConnectableId()
    },
    'RestrictedAreaWithinDistance': {
      distance: 25
    }
  };
  if (componentData) {
    Object.keys(componentData).forEach(function(key) {
      newComponentData[key] = componentData[key];
    });
  }

  addComponentsToEntity(powerstation, ['Connectable', 'Movable', 'Powerstation', 'RestrictedAreaWithinDistance'], newComponentData);
}
