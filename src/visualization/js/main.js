// Boat: https://free3d.com/3d-model/low-poly-tugboat-67766.html
// Compass: https://www.turbosquid.com/FullPreview/Index.cfm/ID/1122479
// Electrical tower: https://free3d.com/3d-model/electricity-tower-25295.html

// Prefix for visualization files
var assetBasePath = '/src/visualization/';

// Will contain the scenes for the project and the compass
var scene, compassScene;

// Will point to the visualization canvas, stats object (FPS estimator) and statsContainer (FPS window container in DOM)
var canvas, stats, statsContainer;

// Global flag whether three js should render at all. Is false whenever some other page (e.g. dashboard) is open in the visualization
var threeJSShouldRender = false;

// Will point to the detail boxes in the frontend
var visualizationDetailBox = null;
var visualizationGlobalDetailBox = null;

// Contains the project that is currently open
var project = null;

// Timestamp of the last render, is used to compute elapsed time since last render -> used for animations and such
var lastTimestamp = -1;

/**
 * Is called from outside (after DOM is loaded, especially the canvas and stats container)
 * Initializes everything that is project-independent (scenes, renderer, stats, camera, controls, 
 * ever-present geometry like ocean/skybox etc.)
 */
function initVisualization(canvasContainer, statsContainer) {
  scene = new THREE.Scene();
  compassScene = new THREE.Scene();

  stats = new Stats();
  stats.showPanel(0);
  statsContainer.appendChild(stats.dom);

  initCamera();
  initRenderer(canvasContainer);
  initLights();
  canvas = renderer.domElement;
  canvasContainer.appendChild(canvas);

  // Init controls and resize/mouse/key events
  initControls();

  // Create ever-present objects
  createOcean();
  createCompass();
  initSkybox();

  // Init debug gui
  initGUI();

  // Invoke window resize event to resize renderer and render buffers
  visu_onWindowResize();
}

/**
 * Render function, called every frame (if tab/window is activated only)
 */
function render(time) {
  var elapsedSinceLastFrame = this.lastTimestamp === -1 ? 0 : time - this.lastTimestamp;
  this.lastTimestamp = time;

  // Subscribe to new frame with render callback
  requestAnimationFrame(render);
  stats.begin();

  // Only if visualization is actually visible
  if (threeJSShouldRender) {
    // If the most important objects exist and updating components is activated in the settings
    if (globalSettings.updateComponentsFlag && scene && project && project.terrain && project.terrain.heightmap && project.terrain.fullyLoaded) {
      entityManager.update(elapsedSinceLastFrame);
    }

    // Reset mouse changed status after the systems have run
    mouse.changed = false;

    // Set sun position according to current time. Uncomment increment of time for animation
    // timeOfDay += 0.005;
    // if (timeOfDay >= 16.0) {
    //   timeOfDay = 8.0;
    // }
    setSunPosition();

    // If the most important objects exist and rendering is activated in the settings
    if (globalSettings.renderFlag && project && project.terrain) { // Render only after grid is loaded
      // Clear the render buffer
      renderer.clear();

      // Render and run post-processing steps
      composer.render();

      // Clear the depth buffer of the renderer
      renderer.clearDepth();

      // Now render the compass with the renderer but with a different scene and camera
      renderer.render(compassScene, compassCamera);
    }
  }

  stats.end();
}
