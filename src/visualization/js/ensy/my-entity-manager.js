/**
 * A proxy object is a 'shadow object' of an object where we can intercept reading and writing on properties
 * 
 * It is used here to set the needsUpdate flag whenever we change some property of the object, even if
 * it's a deep change like Transform.position.x = 2. 
 */
function generateProxyHandler(rootTarget) {
  return {
    get(target, key) {
      if (typeof target[key] === 'object' && target[key] !== null) {
        // To be able to intercept write operations on child properties, e.g. Transform.position.x = 1, we need to return
        // a new proxy recursively for the get operation on a property. We need to pass rootTarget to be able to modify the 
        // root object (change needsUpdate property)
        return new Proxy(target[key], generateProxyHandler(rootTarget));
      } else {
        if (key == 'needsUpdate' && !target.hasOwnProperty(key)) { // If no needsUpdate property is present assume we need an update
          return true;
        } else {
          return target[key]; // Otherwise return the value that is queried (like Transform.position.x)
        }
      }
    },
    set(target, key, value) {
      if (key != 'needsUpdate' && target[key] != value) {
        // On property changes we set the needsUpdate flag on the root target (the component state), but only if it's not the needsUpdate
        // property itself (otherwise if we set it to false, it would automatically be changed to true)
        rootTarget.needsUpdate = true;
      }
      target[key] = value;
      return true; // true=Success
    }
  };
}


/**
 * MyEntityManager extends the ensy EntityManager class with some convenience functions, and returns proxy objects for every
 * component state such that we have a nice needsUpdate property that allows a performant update of the systems
 */
class MyEntityManager extends EntityManager {
  constructor(listener) {
    super(listener);
  }

  /**
   * Return the states for all components componentIds of all entities that have ALL these components
   *
   * Example: entity1 and entity2 have Connectable and Windmill, entity3 has Connectable and Powerstation
   *
   * getAllComponentsData(['Connectable', 'Windmill']) will return 
   * [
   *  {
   *    entityId: 1,
   *    Connectable: {...},
   *    Windmill: {...}
   *  },
   *  {
   *    entityId: 2,
   *    Connectable: {...},
   *    Windmill: {...}
   *  }
   * ]
   */
  getAllComponentsData(componentIds) {
    for (var i = 0; i < componentIds.length; i++) {
      var componentId = componentIds[i];
      if (!(componentId in this.components)) {
        throw new Error('Trying to use unknown component: ' + componentId);
      }
      if (!this.entityComponentData.hasOwnProperty(componentId)) {
        return [];
      }
    }

    // All entities
    var entities = this.entities;

    // Filter for all entities that have all the components
    for (var i = 0; i < componentIds.length; i++) {
      var componentId = componentIds[i];
      entities = entities.filter(entity => {
        var entityHasComponent = this.entityComponentData[componentId].hasOwnProperty(entity);
        return entityHasComponent;
      });
    }

    return entities.map(entity => {
      var dataObj = {
        entityId: entity,
        getSceneGraphObject: function() { // Convenience function to retrieve the threejs scene graph object of the entity
          return sceneGraphEntities[this.entityId];
        }
      };
      for (var i = 0; i < componentIds.length; i++) {
        var componentId = componentIds[i];
        // dataObj[componentId] = this.entityComponentData[componentId][entity]; // Old version, just returning the state
        dataObj[componentId] = new Proxy(this.entityComponentData[componentId][entity], generateProxyHandler(this.entityComponentData[componentId][entity])); // New version: return a proxy to monitor changes
      }
      return dataObj;
    });
  }

  /**
   * Returns the component state of componentId for an entity
   */
  getComponentDataForEntity(componentId, entityId) {
    if (!(componentId in this.components)) {
      throw new Error('Trying to use unknown component: ' + componentId);
    }

    if (!this.entityComponentData.hasOwnProperty(componentId) ||
      !this.entityComponentData[componentId].hasOwnProperty(entityId)
    ) {
      throw new Error('No data for component ' + componentId + ' and entity ' + entityId);
    }

    // return this.entityComponentData[componentId][entityId];
    return new Proxy(this.entityComponentData[componentId][entityId], generateProxyHandler(this.entityComponentData[componentId][entityId]));
  }
}
