# TODO

- [x] Controls
- [x] Add windmill foot
- [x] Reactive systems - needsUpdate property
- [x] Erase terrain? Because ground data should contain also land data, so where could we put the horizon?? Maybe ocean all around?
- [x] Stop render on page change
- [x] Camera movement: Move skybox and ocean
- [x] Forbidden area selection
- [x] Move shadow camera with rendering camera
- [x] Cable placement on land? (Mountains??)
- [x] Before creation of windmills/cables: Check wheather they already exist there
- [x] Fix grid
- [x] Change outlinePass size on window resize
- [x] Live data mapping
- [x] Remove global references to heightmap
- [x] Tabs
- [x] Powerstation model
- [x] Clear input fields on submit/cancel etc
- [x] Fix touch scrolling
- [x] Fix touch controls
- [x] Image upload button
- [x] Remove heightFactor, pixelWidth from projectOptions
- [x] Map resize on sidebar toggle
- [x] Map popups
- [x] Make dashboard notice nicer
- [ ] Try polymer bundling with bundle: false?
- [ ] Skybox from http://www.graphic-sim.com/images/sunnysky/ ?
- [ ] Orga dropbox disabled in settings

# Low Priority
- [x] Highlight selected object
- [x] Waterplane bigger
- [ ] Revise project destroy function
- [ ] Color "forward" part of compass needle different/Make it an arrow

# Bugs
- [x] Fix outlinePass
- [x] Fix terrain tile seams
- [ ] Fix compass aspect ratio

## Maybe
- [ ] Windmill instancing
- [ ] ~~Include threejs, ensy via bower?~~
- [ ] Camera entity with target value (needsUpdate variable for moveWithCamera component)
- [ ] Alter needsUpdate at end of frame?
- [ ] ~~Decals for cables on higher ground?~~

# Decisions
- [ ] Pick objects directly (and set new position to mouse position on grid) **vs** move with relative mouse movement **vs** pick grid element and move all objects on it
- [ ] Not being able to place objects where an object already is **vs** window with warnings/errors **vs** both

#Documentation
- [x] Asset links
- [x] Plugins: Datgui, threejs, fps window
- [x] Mapbox: no height data in ocean
- [x] Future: LOD for terrain

#Presentation
15min presentation
