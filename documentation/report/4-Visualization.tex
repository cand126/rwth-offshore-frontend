% !TEX root = report.tex
\section{Visualization}\label{sec:visualization}
\subsection{Overview}
\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=\linewidth, trim=0cm 0cm 3.74cm 0cm, clip=true]{visualization/visualization.pdf}
  \end{center}
  \captionsetup{format=plain}
  \caption{The visualization window. A wind turbine is selected in (a), detailed information about the selection is shown in (b). Global information is shown in~(c). A compass (d) shows the cardinal direction and the wind direction (little flag). A toolbar~(e) allows to switch between different editing modes\label{fig:visualization}}
\end{figure}

\noindent We use the Three.js library to visualize offshore wind farm projects. We load the heightmap and textures of the selected project area dynamically from the web service Mapbox. The objects contained in the project options are loaded and shown with self-explaining meshes. Restricted areas are shown as red areas on the terrain. With a toolbar the user can switch between different editing modes to create more objects, modify or delete them by simple mouse/touch interaction. He can add and remove different types of restricted areas and immediately see the result. Most scene objects are managed with an entity component system, that enables to describe and let them interact in a compact and extensible way.

The visualization enables the user to control most details in a project he created in the frontend in a nice-looking and easy-to-use manner~(see also Figure~\ref{fig:visualization}).

\subsection{Libraries}
We use the library ensy from \url{https://github.com/adngdb/entity-system-js} as a base for our entity component system. Additionally we use node-pngjs \url{https://www.npmjs.com/package/node-pngjs} to parse png images into an accessible object where we can retrieve pixel colors. We use dat.GUI (which was already shipped with three.js), a lightweight menu overlay, from \url{https://github.com/dataarts/dat.gui}, as a debug window to quickly control some values.

\subsection{User Interface}
\subsubsection{Frontend Integration}
\begin{wrapfigure}{r}{0.3\textwidth}
  \vspace{-2cm}
  \begin{center}
    \includegraphics[width=0.1\textwidth]{visualization/toolbar.png}
  \end{center}
  \captionsetup{format=plain}
  \caption{Visualization toolbar. The createPowerstations mode of the project objects group is selected}
\end{wrapfigure}
The User Interface is integrated in the frontend by using several Polymer components. Therefore we have one main component the visualization-component which contains separate Polymer components for the visualization, the toolbar and the different information windows.

The user can interact with the visualization by using the different editing modes which are offered through the toolbar. This changes how clicks and mouse movements are interpreted. The icons used are loaded from the Font Awesome and Material icon libraries.

\subsubsection{Editing Modes}
All editing modes can either be chosen by clicking on the specific icon or by using the key which is mapped to this mode. The first, default editing mode is one where the user can move the camera around and can select objects by clicking on them. The other editing modes are grouped, so that the toolbar is as small as possible and easy to use. By clicking on the group icon, a list of icons pops open, where we can select the different editing modes. Once selected, is it shown as a second yellow icon in front of the group icon.

\begin{sidebysidebox}{Editing modes to modify the wind park objects}{4cm}
  \begin{itemize}
    \item Movement\\
    Allows the user to move already placed windmills or powerstations. The connected cables will move along the moved object.
    \item Placing a windmill\\
    Allows the user to place windmills on the grid. To make sure that windmills can't interfere with each other each placed windmill restricts a circular area around it. 
    \item Placing a powerstation\\
    Allows the user to place powerstations on the grind.
    \item Placing cables.\\
    Allows the user to connect windmills and powerstations, by selecting an object as a starting point and another as the endpoint.
    \item Remove objects.\\
    Allows the user to remove objects by clicking on them. 
    \end{itemize}

  \tcblower
  \includegraphics[width=\linewidth]{visualization/toolbarObjects.png}
\end{sidebysidebox}

\begin{sidebysidebox}{Editing modes to modify restricted areas}{4cm}
  \begin{itemize}
  \item Selecting a polygon.\\
  Allows the user to restrict an area by choosing an area on the map. This can be done by selecting the corner points of the polygon which will be restricted.
  \item Placing a restricted circular area\\
  Allows the user to restrict a circular area by choosing the center point of the circle and then adjusting the radius of the circle.
   \item Remove restricted areas.\\
  Allows the user to delete restricted areas by clicking on them.
  \end{itemize}

  \tcblower

  \includegraphics[width=\linewidth]{visualization/toolbarRestrict.png}
\end{sidebysidebox}

\noindent The editing modes are implemented by extending an abstract EditingMode class and overriding class methods like initMode, exitMode, mouseDown, mouseMove, mouseUp, touchDown, touchMove, touchUp or mouseScrolled. For a mode change, the exitMode method of the old mode is called first, then initMode of the new one. All mouse and touch events first prepare a mouse object with all necessary information, then passes it to the respective method of the active mode.

\subsubsection{Settings Window}
\begin{sidebysidebox}{Settings Window}{7cm}
  Allows the user to activate the loading of live data. Furthermore it it possible to hide the grid, the seaplane, the box with global information. 

  \tcblower

  \includegraphics[width=\linewidth, trim=0cm 0cm 0cm 5.7cm, clip=true]{visualization/settingsWindow.png}
\end{sidebysidebox}

\subsubsection{Camera Control}
The user can click and drag the left mouse button/touch drag to rotate the camera or he can double click onto the map to move the camera so that it looks at the selected position.

\subsection{Three.js}
Threejs provides functionality for almost all aspects of rendering. Objects are managed in a scene graph (a tree where every node has a Transform matrix consisting of position/scale/rotation) so that the world pose of every object is determined prior to rendering by traversing the tree and setting an objects transform matrix to the multiplication of all ancestor matrices. That allows objects to be placed relative to each other, e.g. a windmill consists of the tree\\
\\
\begin{center}
\begin{tikzpicture}[sibling distance=6em,
  every node/.style = {shape=rectangle, rounded corners,
    draw, align=center,
    top color=white, bottom color=blue!20}]]
  \node {Windmill}
    child { node {Basis} }
    child { node {Rotatable Group}
      child { node {Wing group}
        child { node {Wing} }
        child { node {Wing} }
        child { node {Wing} }
      }
    };
\end{tikzpicture}
\end{center}

\noindent Here windmill has the windmill position as position attribute. The basis is a box below the tower. Rotatable Group is the actual tower, it has the position $(0,0,0)$ (in world space it is at the windmill position), but has a rotation around the z axis so that it can be aligned with the wind direction. The wing group contains the three wings and has as position the relative position of the tower head, a little shifted towards the front. The wings again only have a rotation around the y axis (each shifted by $120^\circ$), which will be incremented each frame to visualize the wind speed.

Furthermore we use some extra libraries of threejs. We use a contour multistage render pass to highlight hovered or selected objects. We use the module OrbitControls for moving the camera with mouse and touch input. We use the modules OBJLoader and MTLLoader to load .obj files and their .mtl material files. We also use the module FXAAShader for a final antialiasing pass.

\subsection{Project Loading}
All project options regarding the visualization (in comparison to name/description/image etc. which is dealt with only in the frontend) are loaded from a json file like this:
% \begin{wrapfigure}{r}{0.5\textwidth}
  % \begin{simplebox}{Test}
  \begin{lstlisting}[language=json]
  {
    "terrainOptions": {
      "type": "mapbox",
      "boundingPolygon": [
        {
          "lat": -34.04060105332334,
          "long": 18.527100036981693
        },
        {
          "lat": -34.107925723245,
          "long": 18.55275525445751
        },
        {
          "lat": -34.128040475244575,
          "long": 18.46806675342586
        }
      ],
      "tileOptions": {
        "zoom": 14,
        "resolution": 64,
        "texResolution": 256
      },
      "gridSpacing": 100,
      "heightSubtractor": -2.5,
      "restrictedAreas": [{
        "type": "polygon",
        "coords": [{
          "lat": 53.541965,
          "long": 9.968920
        }, {
          "lat": 53.540744,
          "long": 9.972940
        }, {
          "lat": 53.538697,
          "long": 9.966196
        }]
      }, {
        "type": "circle",
        "center": {
          "lat": 53.542965,
          "long": 9.968920
        },
        "radius": 100.0
      }]
    },
    "objects": [{
      "type": "Windmill",
      "id": 1,
      "x": 59,
      "y": 25,
      "efficiency": 0.6,
      "liveDataId": "abc"
    }, {
      "type": "Powerstation",
      "id": 2,
      "x": 57,
      "y": 22,
      "efficiency": 0.7
    }, {
      "type": "CableConnection",
      "pointA": 1,
      "pointB": 2
    }]
  }
  \end{lstlisting}
  % \end{simplebox}
% \end{wrapfigure}

\subsubsection{Terrain Loading}
\begin{wrapfigure}{R}{0.45\textwidth}
  \vspace{-1cm}
  \begin{center}
    \includegraphics[width=\linewidth, trim=1.2cm 1.7cm 10.4cm 1cm, clip=true]{visualization/tiles.pdf}
  \end{center}
  \captionsetup{format=plain}
  \caption{All tiles within a rectangular enclosing the drawn polygon are loaded and shown in the visualization. The polygon is used in addition to mark the region where objects can be placed (gray area)\label{fig:tilepolygon}}
\end{wrapfigure}
The first thing defining the project is the place on earth it is located at. The \allowbreak|terrainOptions| property contains a polygon of coordinates defining the area that is important for a project. The visualization renders a rectangular terrain that covers this polygon (see Figure~\ref{fig:tilepolygon}). The terrain is, apart from its dimensions, defined by the height at each point within the project bounds.

This height data is fetched from the online maps provider Mapbox. Mapbox can provide high definition height maps for land and some underwater depth data. Mapbox, as other map services tile the earth at different zoom levels, with a tile size of 256x256 pixels. At zoom level 0, the world is covered by 1 tile, at level 1 it is covered by 2x2 tiles and so on. The zoom level of the project \allowbreak|terrainOptions.tileOptions.zoom| has to be set by the user. Our application loads all tiles within the bounding polygon at the given zoom level, for the height data with a resolution of \allowbreak|terrainOptions|\allowbreak|.tileOptions.resolution|, for the texture with \allowbreak|terrainOptions|\allowbreak|.tileOptions|\allowbreak|.texResolution|. The terrain then combines all height maps to one twodimensional heightmap array, but renders it as separate tiles with their textures. A large transparent plane with a generic ocean texture is placed at $z=0$ to visualize the sea level. Mapbox does not return very diverse ocean depths, thus sometimes gives depth values of around 0 meters, resulting in these areas not actually being covered by water. A pure aesthetical addition is the property \allowbreak|terrainOptions.heightSubtractor|, which defines how many meters the whole terrain (not the ocean plane) is lowered to make the sub-zero areas actually sub-zero.

\subsubsection{Coordinate Conversions}
Our application uses different kinds of coordinates. First, the project bounds are defined by spherical coordinates. The upper left point of the overall heightmap is defined as origin for a world space coordinate system with a unit length of 1 meter. Additionally a grid (also starting at the world space origin) is defined by \allowbreak|terrainOptions.gridSpacing|, which defines the distance of two adjacent grid points. Objects can be placed at the intersection points of the grid. 

\subsubsection{Project Objects}\label{sec:ProjectObjectLoading}
Different objects like wind turbines and power stations can be placed on the grid, ob project load we take the provided grid position of the objects. They are set to $z=0$ on the sea plane (with a foot reaching to the sea ground) and appear at the correct height on higher ground. Windmills and power stations can have the exemplary property efficiency, which does not do anything other than being shown to the user. This can be extended to including more useful measures for each object. Cables can two objects, given by their Connectable id.

\subsubsection{Restricted Areas}
Certain areas of the terrain are forbidden to place objects on. That is for one thing everything outside the polygon bound of the project. Then there is an upper and lower bound for heights objects can be placed on (hardcoded for now). 

In addition to these fixed restrictions, the user can create restricted polygons and circles with the respective editing modes. They are loaded from the \allowbreak|terrainOptions|\allowbreak|.restrictedAreas| array.

\subsection{Entity Component System}
The heart of managing all the objects in the scene is an entity component system (ECS). An ECS is a way to avoid the limitations of class inheritance. For this paradigm components form traits an entity can have. A component has a state containing data, but no logic. The ECS holds a list of entities, which components each has and the respective states. 

Systems are functions that operate on these entities. They usually retrieve all entities that have a certain component/multiple components and modify the data.

One example is the \allowbreak|GridPosition| component. It holds two integer coordiates of the entity on the grid. The \allowbreak|GridPositionSystem| retrieves all entities that have the \allowbreak|GridPosition| component and the \allowbreak|Transform| component. It then computes the world space coordinates of the grid position and updates the position vector of the \allowbreak|Transform| component, which is later used to render the object. 

In the creation editing mode, one such object is additionally equipped with the \allowbreak|GridPositionByMousePosition| component.
The \allowbreak|GridPositionByMouse|\\|PositionSystem| takes the current mouse position on the screen, projects a ray to the scene and intersects it with the ground plane. It then writes the the nearest grid position to the entities \allowbreak|GridPosition|. As a result the entity follows the mouse cursor, while snapping to the grid. When the user places the object in the scene by clicking, the \allowbreak|GridPositionByMousePosition| component is removed and the entity stays in place.

We have used the JavaScript ECS library ensy that provided the base functionality of an ECS. We have extended the EntityManager class by some additional functionality.

\subsection{Components}
We have implemented a number of components, some descriptions already contain what the respective system does:

\begin{simplebox}{Components}
  {\setlength{\extrarowheight}{0.3cm}%
  \begin{longtable}{p{0.4\linewidth}p{0.5\linewidth}}
    Transform & Holds the transform (position, scale and rotation vectors) applied to the object of the scene graph. \\
    GridPosition & Holds 2D integer grid position\\
    GridPositionByMousePosition & Intersects mouse ray with terrain to set the grid position\\
    TransformAnimation & Holds a transform, whose values $\cdot \Delta t$ are added to the Transform component\\
    TextureAnimation & Holds uv-coordinates which a are ($\cdot \Delta t$) added to the texture uv coordinates\\
    MoveTextureWithWind & Configures TextureAnimation such that the texture of an axis oriented plane will move with the global wind direction and speed\\
    RotateWithWindspeed & Configures TransformAnimation such that the object rotates around a given axis with the given speed (characteristic wind turbine rotation function given the wind speed)\\
    RotateWithGlobalWindspeed & Configures RotateWithWindspeed to rotate with the global wind speed\\
    RotateWithWindDirection & Changes Transform such that the object is rotated around a given axis to match the wind direction\\
    CastShadow & Flag whether the object should cast a shadow\\
    ReceiveShadow & Flag whether the object should receive shadows\\
    Movable & Flag whether the object can be moved around in the moving editing mode\\
    Selectable & Flag whether the object can be selected by clicking on it\\
    Connectable & Flag whether the object can be connected by a cable, holds a connectable id\\
    Line & Holds end points (in world space), a line is created that shows a connection between these points\\
    CableConnection & Holds end points (connectable ids)\\
    Mesh & Holds filenames for an .obj and a .mat file\\
    Plane & Adds a 1x1 plane as a child to the scene graph object\\
    Circle & Adds a radius 1 circle as a child to the scene graph object\\
    Box & Adds a 1x1x1 cube as a child to the scene graph object\\
    Material & Holds material properties color, transparent and opacity, which get applied to all scene graph children\\
    Windmill & Holds windmill specific data which is shown in a detail box in the frontend\\
    Powerstation & Holds power station specific data which is shown in a detail box in the frontend\\
    ExtendToGround & Transforms the Transform component such that the object reaches from the heightmap height at the objects grid position to the minimum height at the neighboring grid positions. Used for the windmill/powerstation foot to create the impression of a solid basis on the ground\\
    MoveWithCamera & Sets the Transform position to the point the camera is currently looking at, used for the skybox, the ocean plane etc. so that they always appear the same\\
    RelativeToCamera & Used only for the compass. Holds a screen position, the object is placed at the projection of this screen position to world space at a distance of 0.5m\\
    RotateWithCameraDirection & Used only for the compass. Rotates the entity around a given axis by the negative camera z rotation. Used to rotate the compass, which lives in the separate space so that it always points north\\
    ColorCodeObjectPlaceability & Sets the Material color so that the entity is red if its grid position is restricted and green otherwise\\
    RestrictedAreaWithinDistance & Indicates that the area within a given distance is restricted\\
    WindDistribution & Holds angle/speed pairs of a wind distribution, can be shown as graph on windmill foot\\
    LiveData & Holds a live id to assign live data to the entity
  \end{longtable}}
\end{simplebox}

\subsection{Systems}
The systems (executed in this order) working on these components are: \\

\begin{simplebox}{Systems}
  {\setlength{\extrarowheight}{0.3cm}%
  \begin{longtable}{p{0.4\linewidth}p{0.5\linewidth}}
    SettingsSystem & Updates some properties from the global settings, e.g. the visibility of the terrain/the seaplane according to a flag\\
    GridPositionSystem\\
    ExtendToGroundSystem\\
    SceneGraphUpdateSystem & Transfers the position/scale/rotation of Transform to the scene graph object, as well as castShadow/receiveShadow and Material properties to all scene graph children\\
    TransformAnimationSystem\\
    TextureAnimationSystem\\
    RotateWithGlobalWindspeedSystem\\
    RotateWithWindspeedSystem\\
    RotateWithWindDirectionSystem\\
    RotateWithCameraDirectionSystem\\
    RelativeToCameraSystem\\
    MoveTextureWithWindSystem\\
    InitMeshSystem & Loads the mesh (if not already loaded) and adds it as child of the scene graph object\\
    LineSystem & Creates a line (if not already created) in form of a slim plane rotated so that it connects the points\\
    CableConnectionSystem\\
    InitPlaneSystem\\
    InitCircleSystem\\
    InitBoxSystem\\
    GridPositionByMousePositionSystem\\
    MoveWithCameraSystem\\
    ColorCodeObjectPlaceabilitySystem\\
    WindDistributionSystem & Places circle segments around the windmill foot to represent the wind distribution\\
    UIUpdateSystem & Prepares some data to be shown in the frontend as property/value pairs. That is windmill/power station/cable specific data if one is selected, as well as aggregated data like the total cable length
  \end{longtable}}
\end{simplebox}

\noindent To show how the components are used, we return to the example of the windmill object. For each node the attached components are listed\\
\begin{tikzpicture}[sibling distance=13em, level distance=2cm,
  every node/.style = {shape=rectangle, rounded corners,
    draw, align=center,
    top color=white, bottom color=blue!20}]]
  \node {Windmill: Transform, GridPosition, \\CastShadow, Selectable, WindDistribution}
    child { node {Basis: Transform, Box, \\Material, ExtendToGround} }
    child { node at (2.5, 0) {Rotatable Group: Transform, Transform, \\RotateWithWindDirection, Mesh}
      child { node at (-3.5, 0) {Wing group: Transform, TransformAnimation, \\RotateWithWindspeed, RotateWithGlobalWindspeed}
        child { node {Wing: Transform, Mesh} }
        child { node {Wing: Transform, Mesh} }
        child { node {Wing: Transform, Mesh} }
      }
    };
\end{tikzpicture}

\subsubsection{Optimized System Update}
Most of the systems transforms input data to output data, like the grid position to the world space position. The update does not have to take place if the input data has not changed. Therefore every component has a needsUpdate flag that indicates whether the data has changed. This is implemented in a generic way by handling the component states as proxy objects with modified get and set handlers that recursively return proxies as proxies and set the needsUpdate flag if we change a property to a value \textbf{different} from the original value. 

This allows the following:

\begin{lstlisting}[language=JavaScriptWithOutput]
entity.GridPosition.position.x
> 10
entity.needsUpdate
> false
entity.GridPosition.position.x = 10
entity.needsUpdate
> false
entity.GridPosition.position.x = 11
entity.needsUpdate
> true
\end{lstlisting}

Another example is the Mesh component. The InitMeshSystem checks the needs\-Update flag of Mesh, which is always true for all components in the first frame, loads the mesh and sets Mesh.needUpdate to false. Whenever we change Mesh.filename, needUpdate will switch to true and InitMeshSystem will load the new mesh file to replace the old one.

\subsection{Grid}
Rendering a simple grid with threejs lines is easy but very slow for a reasonably small grid spacing. We avoided using additional geometry for the grid and instead color the existing ocean and terrain geometry in the shader. This is elegant, allows to show the grid also on elevated ground, which is otherwise not possible, and needs only negligible processing time.

The computation takes place in the fragment shader, as the terrain geometry can be sparse and interpolating between vertices cannot achieve the desired effect. Therefore in the fragment shader, which operates in screen space, the pixel positions have to be translated back to world space, which is done with the inverse projection matrix precomputed and provided as uniform. Then the pixel is colored in black if the world space coordinate (on the x/y plane) is near a multiple of the grid spacing, also provided as uniform.

To enable a use like this we had to change the code of threejs to upload the uniforms listed, as well as grid width and grid visibility. We used the standard phong rendering, but exchanged the shaders by our slightly adjusted versions. We did not manage to use the threejs shader material for this extended use.

As we use logarithmic depth buffers, which are very suitable for such a huge scene if we want to render near and far object without z-fighting. In case the graphics card/browser is capable of using the OpenGL USE\_LOGDEPTHBUF\_EXT extension we have enough information in the fragment shader to unproject the pixel position to world space. If not, rendering the grid this way does not work, thus we have to disable logarithmic depth buffers in that case.

\subsection{Live Data}
Exemplary live data integration is implemented, although there is no live data capability in the backend at the moment. The project object accepts live data chunks that have a type (WindDistribution, Direction and Speed at the moment), an id and data matching the type of the chunk. For now these three types can be processed, thus e.g. if a direction chunk is incoming, the entity with the LiveData component and the live data id matching the chunk will get a rotation according to the wind direction and rotate accordingly in the visualization (if live data is activated by the user in the settings).

\subsection{Changes to Three.js}
Apart from the changes necessary for our grid rendering, we changed the OrbitControls so that it can be disabled and enabled (especially in the different editing modes). Also the rotation with touch was not configured correctly. The OutlinePass module was slightly adjusted too, as it was not initially able to outline simple lines (temporarily used for cables and the grid).

\subsection{Compass}
The integrated compass provides the user with the cardinal direction the camera is pointing at, as the needle always points north. The small flag at the center provides the current global wind direction. \\
The compass is loaded in the separate parts. The compass body, the needle and the flag.

\subsection{Assets}
We use a compass model from \url{https://www.turbosquid.com/FullPreview/Index.cfm/ID/1122479} and an electrical tower model from \url{https://free3d.com/3d-model/electricity-tower-25295.html}, slightly modified. The skybox from here \url{https://www.roblox.com/library/433638584/Sea-Skybox}, slightly edited.

We have created the wind turbine ourselves in Blender.

\subsection{Project Serialization}
For saving a project we take the terrainOptions used for loading and update it with the user created restricted areas. We then serialize all created objects with all information explained in the section~\ref{sec:ProjectObjectLoading} and store them in the objects array. On a project save, automatically invoked whenever we create, move or delete something, the frontend is notified to upload the changes.

\subsection{Future Work}
\begin{itemize}
  \item It would be nice to automatically infer a suitable Mapbox zoom level with a feasible number of tiles to load and render
  \item The objects are set on the grid and will stay on their grid position even if we change the grid spacing or the map box zoom level, in world space they will be in different spots then. So for now it is advised to create objects only after zoom level and grid spacing are fine-tuned and will not change anymore
  \item Project error management needs to be revised. It is not possible to create objects in restricted areas, but a restricted area can be placed where objects already stand. The user should at least be notified. Also cables can cross at the moment. This could be also checked and listed, maybe in a window where all the project errors appear. This would be espacially helpful if the user will still be able to see and modify the project json
  % \item The terrain rendering is still rather slow, although there are indeed a lot of points to render, depending on the project size. It is not very optimized though at the moment, so with some changes, e.g. by using buffer geometries, it might be much faster. Also a Level-Of-Detail tree for the tiles could speed things up
  \item We have not implemented a way to open projects in read-only mode. This could be used to view/visualize all projects on the world map, but not alter them. This may require a private/public flag for projects
  \item Maybe there is a way of showing the dead zone/slipstream behind wind turbines in the visualization
  \item We had some issues deploying the application. Building to production mode does not work at the moment due to some dependency issues we did not understand and could not resolve. Loading times are still quite high, but apart from that the application works on a server like heroku
  \item The terrain rendering is quite optimized already, but there could be improvements: We use a segmented plane as base for the terrain. The segments are evenly spaced, thus we could actually only upload the height value per entry to the graphics card and corner point indices, as well as spacing and offset. But this optimization may not work with WebGL. Also there could be used a Level-of-Detail to lower the quality of far tiles. This would lower render effort, but would increase memory consumption, so there would have to be a trade-off
  \item Multiple users editing the same project at the same time is not possible at the moment, we either need a lock mechanism or progressive updates for the project objects (and live updated in the frontend, e.g. with sockets)
  \item Cables are not always shown correctly on elevated ground, as they connect windmills on the shortest distance
\end{itemize}